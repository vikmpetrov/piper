# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
import uuid
import getpass
import json
from subprocess import call


def generate_uuid():
    """
    Generate a unique identifying string.
    :return: a unique string;
    """
    return str(uuid.uuid4())


def get_current_user():
    """
    Get the name of the current user.
    :return: the name of the current user (string);
    """
    return getpass.getuser()


def json_to_string(dictionary):
    """
    Convert a dictionary object to a string.
    :param dictionary: the dictionary to convert to a string;
    :return: a string representation of a dictionary;
    """
    return json.dumps(dictionary).encode('utf8')


def string_to_json(string):
    """
    Convert a string to a dictionary.
    :param string: the string to convert;
    :return: a dictionary;
    """
    try:
        return json.loads(string.decode())
    except AttributeError:
        return json.loads(str(string))


def read_json(path):
    """
    Get the dictionary data contained in a file.
    :param path: the path to the file;
    :return: a dictionary;
    """
    with open(path) as dictFile:
        data = json.load(dictFile)
        return data


def write_json(dict, path):
    """
    Store a dictionary as a JSON file.
    :param dict: the dictionary to store;
    :param path: the path to the JSON file to write;
    :return: True
    """
    output = json.dumps(dict, sort_keys=True, indent=4, separators=(',', ': '))
    f = open(path, 'w+')
    f.write(output)
    f.close()
    return True


def environment_check(env_variable):
    """
    Check if an environment variable exists and if it has a valid value.
    :param env_variable: the name of the environment variable (string);
    :return: True if the environment variable exists and has a valid value, False if it doesn't.
    """
    if env_variable in os.environ and os.environ[env_variable]:
        return True
    return False


def browse_path(path):
    """
    Open a given path in the OS file browser.
    :param path: the (str) path to navigate to;
    :return: None;
    """
    if sys.platform == "win32":
        os.startfile(path)
    else:
        opener = "open" if sys.platform == "darwin" else "xdg-open"
        call([opener, path])
