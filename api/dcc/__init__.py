# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import imp
from piper.config.dcc_config import DCC_API_MODULE_PATH


def load_dcc_api(dcc):
    """
    Load an return a DCC-related API module.
    :param dcc: the name of the related DCC;
    :return: a module object, e.g. api.dcc.maya_api;
    """
    dcc_api_name = '{}_api'.format(dcc)
    dcc_api_file = os.path.join(DCC_API_MODULE_PATH, '{}.py'.format(dcc_api_name))
    return imp.load_source(dcc_api_name, dcc_api_file)
