# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
import maya.cmds as cmds
from piper.api.dcc import base_api
from piper.api.logger import get_logger

SOURCE = 'maya'
LOGGER = get_logger('Piper Maya API')


def open_file(path_to_file):
    """
    Open a specific file.
    :param path_to_file: the full path to the file to open;
    :return: the full name of the opened file as a string if it exists, otherwise - None;
    """
    if os.path.isfile(path_to_file):
        LOGGER.debug('Opening file %s' % path_to_file)
        return cmds.file(path_to_file, open=True, force=True)
    else:
        LOGGER.debug('Could not find file %s' % path_to_file)
        return None


def save_file(file_path):
    """
    Save a file to a specified path.
    :param file_path: the path to save the file to;
    :return: the full name of the saved file as a string;
    """
    if file_path.split('.')[-1] == 'ma':
        file_type = 'mayaAscii'
    else:
        file_type = 'mayaBinary'
    try:
        cmds.file(rename=file_path)
        LOGGER.debug('Saving file %s' % file_path)
        return cmds.file(save=True, type=file_type)
    except RuntimeError as e:
        LOGGER.error('There was a problem trying to save file %s: %s' % (file_path, e))
        return False


def snapshot():
    """
    Save the current file as a snapshot file.
    :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
    """
    snap = base_api.snapshot(dcc_module=sys.modules[__name__])
    if snap:
        cmds.confirmDialog(title='Piper Snapshot', message='Snapshot created!', icon='information')
    else:
        cmds.confirmDialog(title='Piper Snapshot', message='The current file is invalid for a snapshot!', icon='error')
    return snap


def get_current_file_name():
    """
    Get the full path of the file currently opened.
    :return: the full path of the current file (string);
    """
    return cmds.file(q=True, sn=True)


def get_current_file_format():
    """
    Get the current file's format.
    :return: a (str) file format;
    """
    format = base_api.get_current_file_format(dcc_module=sys.modules[__name__])
    if not format:
        format = 'mb'
    return format


def find_node_from_uuid(uuid_attribute, uuid_value, nodes=None):
    """
    Find a node in a list of nodes, given a UUID attribute and its value.
    :param uuid_attribute: the (str) UUID attribute to look for;
    :param uuid_value: the (str) value of the UUID attribute to match;
    :param nodes: a list of nodes to look through;
    :return: a node with a matching attribute/value pair if it exists, otherwise None;
    """
    # If no nodes specified, recurse through all group nodes:
    if not nodes:
        nodes = cmds.ls(type='transform', absoluteName=True)
    existing_publish_node = None
    for node in nodes:
        # Look for the attribute and check its value:
        if cmds.attributeQuery(uuid_attribute, node=node, exists=True):
            if cmds.getAttr('{0}.{1}'.format(node, uuid_attribute )) == uuid_value:
                existing_publish_node = node
                break

    return existing_publish_node


def export_to_published_file(file_entity, published_file_path):
    """
    Export the current file as a published file.
    :param file_entity: the file entity that represents the published file;
    :param published_file_path: the (str) path to save the file to;
    :return: None;
    """
    # Select all valid nodes in the scene:
    cmds.select(clear=True)

    existing_publish_node = find_node_from_uuid(uuid_attribute='piper_resource_uuid',
                                                uuid_value='file_entity.version.resource.uuid')
    empty_file = False
    if existing_publish_node:
        group = existing_publish_node
    else:
        # Group the nodes and tag the group with the file entity UUID:
        cmds.select(all=True)
        try:
            group = cmds.group(n='publish_grp')
        except RuntimeError:
            empty_file = True
            group = cmds.group(n='publish_grp', empty=True)
        cmds.select(group)

        cmds.addAttr(longName='piper_file_uuid', hidden=True, dataType="string")
        cmds.addAttr(longName='piper_version_uuid', hidden=True, dataType="string")
        cmds.addAttr(longName='piper_resource_uuid', hidden=True, dataType="string")

    cmds.setAttr('{}.piper_file_uuid'.format(group), file_entity.uuid, type='string')
    cmds.setAttr('{}.piper_version_uuid'.format(group), file_entity.version.uuid, type='string')
    cmds.setAttr('{}.piper_resource_uuid'.format(group), file_entity.version.resource.uuid, type='string')

    cmds.setAttr('{}.tx'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.ty'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.tz'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.rx'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.ry'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.rz'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.sx'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.sy'.format(group), lock=True, channelBox=False, type='string')
    cmds.setAttr('{}.sz'.format(group), lock=True, channelBox=False, type='string')

    # Export the group as the published file:
    work_file_name = get_current_file_name()
    save_file(published_file_path)

    if empty_file:
        cmds.delete(group)
    else:
        cmds.ungroup(group)  # versions are only grouped in publish files;
    save_file(work_file_name)

    cmds.select(clear=True)


def reference_version_file(published_version_file):
    """
    Reference a file in the scene.
    :param published_version_file: the published file entity to reference;
    :return: the node created from the file reference;
    """
    cmds.file(published_version_file.path, reference=True)


def find_references(root=None, root_nodes=None):
    """
    Find all references in the scene under a given root.
    :param root: a root entity to start the search from;
    :param root_nodes: a list of root nodes to look through;
    :return: a hierarchical dictionary of UUIDs of all references under the given root;
    """
    present_version_uuids = {}
    if root and not root_nodes:
        root_node = find_node_from_uuid(uuid_attribute='piper_version_uuid',
                                        uuid_value=root.uuid)
        if not root_node:
            return present_version_uuids
        root_nodes = cmds.listRelatives(root_node, children=True, fullPath=True)
    elif not root and not root_nodes:
        root_nodes = cmds.ls(assemblies=True, absoluteName=True)

    for node in root_nodes:
        if cmds.attributeQuery('piper_version_uuid', node=node, exists=True):
            present_version_uuids[
                cmds.getAttr('{}.piper_version_uuid'.format(node))] = find_references(
                root_nodes=cmds.listRelatives(node, children=True, fullPath=True))
    return present_version_uuids


def remove_reference(file_entity):
    """
    Remove a reference from the scene.
    :param file_entity: the file entity representing the file reference;
    :return: None;
    """
    for reference in cmds.ls(type='reference', absoluteName=True):
        if cmds.referenceQuery(reference, f=True) == file_entity.path:
            cmds.file(removeReference=True, referenceNode=reference)
            break


def replace_reference(version_item_entity, new_version_file, **kwargs):
    """
    Replace a reference node with a new reference node.
    :param version_item_entity: the version entity of the selected version item;
    :param new_version_file: the newly selected version published file entity be referenced;
    :return: True if successful, otherwise False;
    """
    # Find the target node to replace:
    target_node = find_node_from_uuid(uuid_attribute='piper_version_uuid',
                                      uuid_value=version_item_entity.uuid)
    if not target_node:
        return False

    reference_node = cmds.referenceQuery(target_node, referenceNode=True)
    cmds.file(new_version_file.path, loadReference=reference_node)

    return True
