# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
import nuke
import nukescripts
from piper.api.dcc import base_api
from piper.api.logger import get_logger
from piper.config.dcc_config import DCC_FILE_EXTENTIONS

SOURCE = 'nuke'
LOGGER = get_logger('Piper Nuke API')


def open_file(path_to_file):
    """
    Open a specific file.
    :param path_to_file: the full path to the file to open;
    :return: the full name of the opened file as a string if it exists, otherwise - None;
    """
    if os.path.isfile(path_to_file):
        LOGGER.debug('Opening file %s' % path_to_file)
        nuke.scriptOpen(path_to_file)
        return path_to_file
    else:
        LOGGER.debug('Could not find file %s' % path_to_file)
        return None


def save_file(file_path):
    """
    Save a file to a specified path.
    :param file_path: the path to save the file to;
    :return: the full name of the saved file as a string;
    """
    try:
        LOGGER.debug('Saving file %s' % file_path)
        nuke.scriptSaveAs(file_path)
        return file_path
    except RuntimeError as e:
        LOGGER.error('There was a problem trying to save file %s: %s' % (file_path, e))
        return False


def snapshot():
    """
    Save the current file as a snapshot file.
    :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
    """
    snap = base_api.snapshot(dcc_module=sys.modules[__name__])
    if snap:
        nuke.message('Piper - snapshot created!')
    else:
        nuke.message('Piper - the current file is invalid for a snapshot.')
    return snap


def get_current_file_name():
    """
    Get the full path of the file currently opened.
    :return: the full path of the current file (string);
    """
    try:
        return nuke.scriptName()
    except RuntimeError:
        return None


def get_current_file_format():
    """
    Get the current file's format.
    :return: a (str) file format;
    """
    return base_api.get_current_file_format(dcc_module=sys.modules[__name__])


def find_node_from_uuid(uuid_attribute, uuid_value, nodes=None):
    """
    Find a node in a list of nodes, given a UUID attribute and its value.
    :param uuid_attribute: the (str) UUID attribute to look for;
    :param uuid_value: the (str) value of the UUID attribute to match;
    :param nodes: a list of nodes to look through;
    :return: a node with a matching attribute/value pair if it exists, otherwise None;
    """
    # If no nodes specified, recurse through all group nodes:
    if not nodes:
        nodes = nuke.allNodes('Group', recurseGroups=True)
    existing_publish_node = None
    for node in nodes:
        # Look for the attribute and check its value:
        if node.knob(uuid_attribute):
            if node[uuid_attribute].value() == uuid_value:
                existing_publish_node = node
                break

    return existing_publish_node


def export_to_published_file(file_entity, published_file_path):
    """
    Export the current file as a published file.
    :param file_entity: the file entity that represents the published file;
    :param published_file_path: the (str) path to save the file to;
    :return: None;
    """

    # Select all valid nodes in the script:
    nukescripts.clear_selection_recursive()
    nuke.selectAll()

    existing_publish_node = find_node_from_uuid(uuid_attribute='piper_resource_uuid',
                                                uuid_value=file_entity.version.resource.uuid,
                                                nodes=nuke.selectedNodes())

    for viewer_node in nuke.allNodes('Viewer'):
        viewer_node.setSelected(False)

    if existing_publish_node:
        group = existing_publish_node
    else:
        group = _create_publish_group()

    _tag_publish_group(group=group, file_entity=file_entity)

    # Export the group as the published file:
    nukescripts.clear_selection_recursive()
    group.setSelected(True)
    nuke.nodeCopy(published_file_path)

    nuke.expandSelectedGroup()  # versions are only grouped in publish files;
    nuke.scriptSave()
    nukescripts.clear_selection_recursive()


def _create_publish_group():
    """
    Group the selected nodes and tag the group with a file entity UUID.
    :return: a group node;
    """
    group = nuke.collapseToGroup()

    file_uuid_knob = nuke.nuke.String_Knob('piper_file_uuid', 'Piper File UUID')
    file_uuid_knob.setVisible(False)
    group.addKnob(file_uuid_knob)

    version_uuid_knob = nuke.nuke.String_Knob('piper_version_uuid', 'Piper Version UUID')
    version_uuid_knob.setVisible(False)
    group.addKnob(version_uuid_knob)

    resource_uuid_knob = nuke.nuke.String_Knob('piper_resource_uuid', 'Piper Resource UUID')
    resource_uuid_knob.setVisible(False)
    group.addKnob(resource_uuid_knob)
    return group

def _tag_publish_group(group, file_entity):
    """
    Set a group's UUID attributes to correspond to the relevant file entity.
    :param group: the group node;
    :param file_entity: the published file entity;
    :return: None;
    """
    group['piper_file_uuid'].setValue(file_entity.uuid)
    group['piper_version_uuid'].setValue(file_entity.version.uuid)
    group['piper_resource_uuid'].setValue(file_entity.version.resource.uuid)
    group['name'].setValue('{0}_{1}'.format(file_entity.name, file_entity.version.name))
    group['lock_connections'].setValue(True)  # don't allow editing the nodes inside the published file;


def reference_version_file(published_version_file):
    """
    Reference a file in the scene.
    :param published_version_file: the published file entity to reference a file from;
    :return: the node created from the file reference;
    """
    _deactivate_nodes()
    nukescripts.clear_selection_recursive()
    if published_version_file.format not in DCC_FILE_EXTENTIONS[SOURCE]:
        if published_version_file.range:
            read_node = nuke.createNode('Read')
            read_node.knob('file').fromUserText('{0} {1}-{2}'.format(published_version_file.path,
                                                                     published_version_file.range[0],
                                                                     published_version_file.range[1]))
        else:
            read_node = nuke.nodes.Read(file=published_version_file.path)
        read_node.setSelected(True)
        group = _create_publish_group()
        _tag_publish_group(group=group, file_entity=published_version_file)
        return group
    else:
        return nuke.nodePaste(published_version_file.path)


def find_references(root=None):
    """
    Find all references in the scene under a given root.
    :param root: a root entity to start the search from;
    :return: a hierarchical dictionary of UUIDs of all references under the given root;
    """
    present_version_uuids = {}
    # Get the relevant nodes to look through:
    if root:
        root_node = find_node_from_uuid(uuid_attribute='piper_version_uuid',
                                        uuid_value=root.uuid,
                                        nodes=nuke.allNodes('Group', recurseGroups=True))
        if not root_node:
            return present_version_uuids
        root_node.begin()

    nodes = nuke.allNodes('Group')
    if root:
        root_node.end()

    # Recursively find all references under each version node:
    for node in nodes:
        if node.knob('piper_version_uuid'):
            node.begin()
            present_version_uuids[node['piper_version_uuid'].value()] = find_references()
            node.end()

    return present_version_uuids


def remove_reference(file_entity):
    """
    Remove a reference from the scene.
    :param file_entity: the file entity representing the file reference;
    :return: None;
    """
    node = nuke.toNode(file_entity.name)
    if node:
        nuke.delete(node)


def _deactivate_nodes():
    """
    Make sure there are no active nodes.
    :return: None;
    """
    for node in nuke.allNodes('Group', recurseGroups=True):
        node.end()
    nukescripts.clear_selection_recursive()

def replace_reference(version_item_entity, parent_item_entity, new_version_file):
    """
    Replace a reference node with a new reference node.
    :param version_item_entity: the version entity of the selected version item;
    :param parent_item_entity: the version entity of the selected version item's parent;
    :param new_version_file: the newly selected version published file entity to be referenced;
    :return: True if successful, otherwise False;
    """
    _deactivate_nodes()

    # Find the target node to replace:
    target_node = find_node_from_uuid(uuid_attribute='piper_version_uuid',
                                      uuid_value=version_item_entity.uuid)
    if not target_node:
        return False

    # Find the parent entity, under which the replacement will happen:
    if parent_item_entity:
        parent_node = find_node_from_uuid(uuid_attribute='piper_version_uuid',
                                          uuid_value=parent_item_entity.uuid)
        if not parent_node:
            return False
        parent_node.begin()

    # Copy the position, inputs and outputs of the old reference node to the new reference node:
    new_node = reference_version_file(new_version_file)
    target_position = (target_node.xpos(), target_node.ypos())
    input_nodes = []
    output_nodes = []
    for i in range(target_node.inputs()):
        input_nodes.append((i, target_node.input(i)))

    for dependant_node in nuke.dependentNodes(nuke.INPUTS | nuke.HIDDEN_INPUTS, target_node):
        for i in range(dependant_node.inputs()):
            if dependant_node.input(i) == target_node:
                output_nodes.append((i, dependant_node))
                dependant_node.setInput(i, None)
    new_node['xpos'].setValue(target_position[0])
    new_node['ypos'].setValue(target_position[1])
    nuke.delete(target_node)

    for input_node in input_nodes:
        new_node.setInput(input_node[0], input_node[1])
    for output_node in output_nodes:
        output_node[1].setInput(output_node[0], new_node)

    if parent_item_entity:
        parent_node.end()
    return True
