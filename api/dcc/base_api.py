# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.config.dcc_config import DCC_FILE_EXTENTIONS


def snapshot(dcc_module):
    """
    Save the current file as a snapshot file.
    :param dcc_module: the child DCC api module to use to run required helper functions;
    :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
    """
    current_file = dcc_module.get_current_file_name()
    if not current_file:
        dcc_module.LOGGER.debug('Current file is not valid for creating a snapshot.')
        return None
    file_name = current_file.replace('\\', '/').split('/')[-1]  # Get the name of the file only.
    file_extension = '.{}'.format(file_name.split('.')[-1])  # Get the file extension only.

    file_directory = current_file.replace(file_name, '')  # Get the file's directory only.

    # Check if we are in the '_snapshots' directory for this file:
    if '_snapshots' not in file_directory:
        # If not - append it to the current path and create it if it doesn't exist:
        snapshot_directory = os.path.join(file_directory, '_snapshots')
        if not os.path.isdir(snapshot_directory):
            dcc_module.LOGGER.debug('Creating snapshot directory %s' % snapshot_directory)
            os.makedirs(snapshot_directory)
    else:
        snapshot_directory = file_directory

    # Check if the current file is a snapshot file based on its naming
    # and determine what part of the file name to replace:
    if '-snap-' in file_name:
        replace = '-snap-{}'.format(file_name.split('-snap-')[-1])
    else:
        replace = file_extension

    # Check for existing snapshot files and create the name for the new snapshot file:
    existing_snapshots = [x for x in os.listdir(snapshot_directory)
                          if x.split('.')[-1] in DCC_FILE_EXTENTIONS[dcc_module.SOURCE]]
    new_snapshot = file_name.replace(replace,
                                     '-snap-{0}{1}'.format(str(len(existing_snapshots)+1).zfill(4), file_extension))

    snapshot_file = os.path.join(snapshot_directory, new_snapshot)
    dcc_module.LOGGER.debug('Creating snapshot %s' % snapshot_file)
    return dcc_module.save_file(snapshot_file)


def get_current_file_format(dcc_module):
    """
    Get the format of the current open file.
    :param dcc_module: a DCC-specific API module;
    :return:
    """
    file_format = DCC_FILE_EXTENTIONS[dcc_module.SOURCE][0]
    current_file = dcc_module.get_current_file_name()
    if current_file:
        file_format = current_file.split('.')[-1]
    return file_format
