# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from piper.config.db_config import DATABASE_URL

# Database connection prerequisites setup:
ENGINE = create_engine(DATABASE_URL)
SCOPED_SESSION = scoped_session(sessionmaker(bind=ENGINE))


class DbConnection(object):
    """
    An object that connects to the database by creating a scoped session.
    """
    def __init__(self):
        self.session = SCOPED_SESSION()
