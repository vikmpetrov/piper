# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.api.utils import read_json


class Profiler(object):
    """
    An object to handle functionality related to software profiles.
    """
    def __init__(self):
        super(Profiler, self).__init__()
        self.profiles = dict()  # To store profile names and file paths.
        # Set the profile files directory:
        self.profiles_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'studio', 'profiles'))
        self.find_profiles()

    def find_profiles(self):
        """
        Find and store the names and paths to all profile files.
        :return: None
        """
        for profile in [j for j in os.listdir(self.profiles_path) if j.lower().endswith('.json')]:
            profile_name = profile.lower().split('.json')[0]
            self.profiles[profile_name] = os.path.abspath(os.path.join(self.profiles_path, profile))

    def get_profile_data(self, profile_name):
        """
        Extract the data from a given profile.
        :param profile_name: the name of the profile file;
        :return: a dictionary containing profile data;
        """
        profile_path = self.profiles[profile_name]
        return read_json(path=profile_path)
