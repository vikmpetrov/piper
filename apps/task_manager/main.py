# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
import os
from piper.config import ICONS_PATH
from piper.gui.qt import QMainWindow, QApplication, QAbstractItemView, QTableWidgetItem, QIcon, load_qt_ui
from piper.gui.widgets.context_widget import ContextWidget
from piper.gui.widgets.create_task import CreateTask
from piper.api.database_api import DbApi
from piper.api.logger import get_logger
from piper.gui.utils import error_message

TASKS_TABLE_HEADER = ['ID', 'Name', 'Description', 'Pipeline Step',
                      'Status', 'Start Date', 'Due Date', 'Artist', 'Hours Logged', 'Comment']


class TaskManager(QMainWindow):
    """
    A class to manage scene assembly in a given DCC.
    """
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        load_qt_ui('task_manager.ui', self)
        self.logger = get_logger('Piper Task Manager')
        self.db_api = DbApi()
        self.user = self.db_api.get_current_artist()
        if self.user.is_administrator:
            self.assembly_context = False  # tells the ContextWidget if this window supports assembly context widgets;
            self.context_widget = ContextWidget(parent=self)
            self.table_index_to_task = {}
            self._setup_data()
            self._connect_widget_cmds()
            self.selected_task = None

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """
        self.contextLayout.addWidget(self.context_widget)
        self.tasksTableWidget.setColumnCount(len(TASKS_TABLE_HEADER))
        self.tasksTableWidget.setHorizontalHeaderLabels(TASKS_TABLE_HEADER)
        self.tasksTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.load_tasks()

    def load_tasks(self):
        """
        Load all tasks associated with the current asset/shot.
        :return: None;
        """
        self.tasksTableWidget.clearContents()
        self.tasksTableWidget.setRowCount(0)
        project = self.context_widget.current_project
        entity = self.context_widget.current_entity
        if not (project and entity):
            return
        asset = None
        shot = None
        if entity.type.lower() == 'asset':
            asset = entity
        else:
            shot = entity
        self.db_api.filters = {'project': project, 'shot': shot, 'asset': asset}
        tasks = self.db_api.get_entities(entity='Task', filtered=True)

        if tasks:
            self.tasksTableWidget.setRowCount(len(tasks))
            for i, task in enumerate(tasks):
                self.table_index_to_task[i] = task
                self._add_task_to_table_row(row_index=i, task=task)
            self.tasksTableWidget.resizeColumnsToContents()

    def _add_task_to_table_row(self, row_index, task):
        """
        Add a table row representation of a task entity.
        :param row_index: the row index of the table widget;
        :param task: a task entity;
        :return: None;
        """
        self.tasksTableWidget.setItem(row_index, 0, QTableWidgetItem(str(task.id)))
        self.tasksTableWidget.setItem(row_index, 1, QTableWidgetItem(task.label))
        self.tasksTableWidget.setItem(row_index, 2, QTableWidgetItem(task.description))
        self.tasksTableWidget.setItem(row_index, 3, QTableWidgetItem(task.pipeline_step.label))
        self.tasksTableWidget.setItem(row_index, 4, QTableWidgetItem(task.status.label))
        self.tasksTableWidget.setItem(row_index, 5, QTableWidgetItem(task.start_date.strftime("%Y-%m-%d")))
        self.tasksTableWidget.setItem(row_index, 6, QTableWidgetItem(task.end_date.strftime("%Y-%m-%d")))
        self.tasksTableWidget.setItem(row_index, 7, QTableWidgetItem(task.artist.label))
        self.tasksTableWidget.setItem(row_index, 8, QTableWidgetItem('{} hrs'.format(task.hours_logged)))
        self.tasksTableWidget.setItem(row_index, 9, QTableWidgetItem(task.comment))

    def create_task(self):
        """
        Run a create task dialog.
        :return: None;
        """
        current_project = self.context_widget.current_project
        current_entity = self.context_widget.current_entity
        if current_project and current_entity:
            create_task_dialog = CreateTask(parent=self, project=current_project, entity=current_entity)
            create_task_dialog.exec_()
            self.load_tasks()
        else:
            error_message(parent=self, msg='No valid project and/or entity selected!')

    def edit_task(self, index, *args):
        """
        Run an edit task dialog.
        :param index: the internal table index corresponding to a task;
        :param args: any other irrelevant arguments;
        :return: None;
        """
        self.selected_task = self.table_index_to_task[index]
        edit_task_dialog = CreateTask(parent=self,
                                      project=self.selected_task.project,
                                      entity=self.selected_task.parent,
                                      edit_mode=True)
        edit_task_dialog.exec_()
        self._add_task_to_table_row(row_index=index, task=self.selected_task)

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.context_widget.context_changed.connect(self.load_tasks)
        self.createTaskBtn.clicked.connect(self.create_task)
        self.tasksTableWidget.cellDoubleClicked.connect(self.edit_task)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show():
    """
    Show the UI.
    :return: None;
    """
    parent_window = QApplication.activeWindow()
    task_manager = TaskManager()
    if not task_manager.user.is_administrator:
        error_message(parent=parent_window,
                      msg='You do not have the administrative permissions to use Task Manager, sorry!')
        return task_manager.deleteLater()
    task_manager.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'task_manager.png')))
    task_manager.setWindowTitle('Piper Task Manager {}'.format(__version__))
    task_manager.show()


if __name__ == '__main__':
    run()
