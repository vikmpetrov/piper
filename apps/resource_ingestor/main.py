# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
import os
from piper.config import ICONS_PATH
from piper.gui.qt import QMainWindow, QApplication, QIcon, load_qt_ui
from piper.gui.widgets.context_widget import ContextWidget
from piper.gui.widgets.file_selector import FileSelector
from piper.gui.widgets import log_work
from piper.apps.publisher import Publisher
from piper.api.database_api import DbApi
from piper.api.logger import get_logger
from piper.gui.utils import browse_file_dialog, error_message, warning_message, info_message, set_combo_box_text


class ResourceIngestor(QMainWindow):
    """
    An object to manage scene assembly in a given DCC.
    """
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        load_qt_ui('resource_ingestor.ui', self)
        self.logger = get_logger('Piper Resource Ingestor')
        self.db_api = DbApi()
        self.context_widget = ContextWidget(parent=self)
        self._data = {
            'tasks': {},
            'resources': {},
            'pipeline_step': {}
        }  # To store label-to-entity references.
        self.file_selector = FileSelector(parent=self)
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """
        self.filesLayout.addWidget(self.file_selector)
        self.contextLayout.addWidget(self.context_widget)
        self.resourceLineEdit.setEnabled(False)
        self._populate_from_context()

    def _populate_pipeline_steps(self):
        if self.context_widget.current_entity:
            self.pipelineStepComboBox.clear()
            self.db_api.filters['level'] = self.context_widget.current_entity.type.lower()
            for pipeline_step in self.db_api.get_entities(entity='PipelineStep', filtered=True):
                self._data['pipeline_step'][pipeline_step.label] = pipeline_step
                self.pipelineStepComboBox.addItem(pipeline_step.label)

    def _populate_existing_resources(self):
        self.resourceComboBox.clear()
        selected_task = self.taskComboBox.currentText()

        if not selected_task:
            return

        task = self._data['tasks'][selected_task]

        for resource in task.resources:
            self._data['resources'][resource.label] = resource
            self.resourceComboBox.addItem(resource.label)

    def _populate_tasks(self):
        self.taskComboBox.clear()
        current_project = self.context_widget.current_project
        current_entity = self.context_widget.current_entity
        if current_project and current_entity:
            pipeline_step = self._data['pipeline_step'][self.pipelineStepComboBox.currentText()]
            asset = None
            shot = None
            if pipeline_step.level == 'asset':
                asset = current_entity
            else:
                shot = current_entity
            self.db_api.filters = {'pipeline_step': pipeline_step, 'shot': shot, 'asset': asset}
            tasks = self.db_api.get_entities(entity='Task', filtered=True)
            for task in tasks:
                self._data['tasks'][task.label] = task
                self.taskComboBox.addItem(task.label)

    def _populate_from_context(self):
        self.pipelineStepComboBox.blockSignals(True)
        self._populate_pipeline_steps()
        self.pipelineStepComboBox.blockSignals(False)
        self._populate_tasks()
        self._populate_existing_resources()

    def _get_selected_task(self):
        selected_task = self.taskComboBox.currentText()
        if not selected_task:
            return None
        return self._data['tasks'][selected_task]

    def ingest_resource(self):
        if not self.file_selector.file_drop.files:
            return error_message(parent=self, msg='No valid file selected!')

        artist = self.db_api.get_current_artist()
        if not artist:
            return error_message(parent=self, msg='Invalid artist, user name not found in database.')

        task = self._get_selected_task()
        if not task:
            return error_message(parent=self, msg='No valid task selected!')

        if self.newResourceRadioBtn.isChecked():
            resource_name = self.resourceLineEdit.text()
            if not resource_name:
                return error_message(parent=self, msg='No valid resource name given!')
            resource_name = '{0}_{1}'.format(task.pipeline_step.name, resource_name)

            selected_pipeline_step = self.pipelineStepComboBox.currentText()
            if not selected_pipeline_step:
                return error_message(parent=self, msg='No valid pipeline step selected!')

            resource = self.db_api.create_entity(entity='Resource',
                                                 artist=artist,
                                                 name=resource_name,
                                                 task=task,
                                                 project=task.project,
                                                 source='standalone',
                                                 description=self.descriptionLineEdit.text(),
                                                 comment=self.commentLineEdit.text())

            if resource:
                info_message(parent=self, msg='Created resource {}!'.format(resource_name))
                self._populate_existing_resources()
            else:
                set_combo_box_text(combo_box=self.resourceComboBox, text=resource_name)
                self.existingResourceRadioBtn.click()
                return warning_message(parent=self, msg='Resource {0} for task {1} already exists!'.format(
                                                                                                    resource_name,
                                                                                                    task.name))

        else:
            selected_resource = self.resourceComboBox.currentText()
            if not selected_resource:
                return error_message(parent=self, msg='No valid resource selected!')
            resource = self._data['resources'][selected_resource]

        version = self.db_api.create_entity(entity='Version',
                                            name=self.db_api.get_new_version_name(resource=resource),
                                            artist=artist,
                                            resource=resource,
                                            project=resource.project,
                                            description=self.descriptionLineEdit.text(),
                                            comment=self.commentLineEdit.text())

        info_message(parent=self, msg='Created version {0} of resource {1}!'.format(version.name, resource.name))

        os.environ['PIPER_TASK_UUID'] = task.uuid
        os.environ['PIPER_RESOURCE_UUID'] = resource.uuid
        publisher = Publisher(ui=False)
        publisher.file_selector = self.file_selector
        publisher.publish_files(version=version)
        publisher.file_selector.file_drop.clear()

        return info_message(parent=self, msg='Published version files!')

    def log_work_cmd(self):
        task = self._get_selected_task()
        if task:
            os.environ['PIPER_TASK_UUID'] = task.uuid
            log_work.show()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.context_widget.context_changed.connect(self._populate_from_context)
        self.pipelineStepComboBox.currentIndexChanged.connect(self._populate_tasks)
        self.existingResourceRadioBtn.clicked.connect(lambda: self.resourceLineEdit.setEnabled(False))
        self.newResourceRadioBtn.clicked.connect(lambda: self.resourceLineEdit.setEnabled(True))
        self.ingestResourceBtn.clicked.connect(self.ingest_resource)
        self.taskComboBox.currentIndexChanged.connect(self._populate_existing_resources)
        self.logWorkBtn.clicked.connect(self.log_work_cmd)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show():
    """
    Show the UI.
    :return: None;
    """
    task_manager = ResourceIngestor()
    task_manager.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'resource_ingestor.png')))
    task_manager.setWindowTitle('Piper Resource Ingestor {}'.format(__version__))
    task_manager.show()


if __name__ == '__main__':
    run()
