# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
from datetime import datetime
from difflib import SequenceMatcher
from subprocess import call
from piper.config import ICONS_PATH
from piper.gui.qt import *
from piper.gui.utils import error_message
from piper.config.db_config import CATEGORY
from piper.api.database_api import DbApi
from piper.api.logger import get_logger
from piper.api.utils import get_current_user, browse_path


class Browser(QMainWindow):
    """
    An app for representing and browsing database data.
    """
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        load_qt_ui('browser.ui', self)
        self.logger = get_logger('Piper Browser')
        self.db_api = DbApi()
        self.item_entity_dict = dict()  # To contain UI item-to-entity pairs.
        self.project_dict = dict()  # To contain project name-to-entity pairs.
        self.asset_type_dict = dict()  # To contain asset type name-to-entity pairs.
        self.version_item_dict = dict()  # To contain version UUID-to-item index pairs.
        self.filter_checkboxes = []
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """

        # Add projects to the project combo box:
        self.projectComboBox.addItem('')
        for project_entity in self.db_api.get_entities(entity='Project', filtered=True):
            self.project_dict[project_entity.label] = project_entity
            self.projectComboBox.addItem(project_entity.label)

        # Add asset type checkboxes to the asset type filters area:
        asset_types = self.db_api.get_entities(entity='AssetType', filtered=True)
        for asset_type in asset_types:
            self.asset_type_dict[asset_type.label] = asset_type
            checkbox = QCheckBox(asset_type.label)
            self.filter_checkboxes.append(checkbox)
            checkbox.setChecked(True)
            checkbox.stateChanged.connect(lambda checked, asset_type_id=asset_type.id:
                                          self.filter_tree_items(attribute='asset_type_id',
                                                                 value=asset_type_id,
                                                                 check_state=checked))
            self.assetTypeFiltersLayout.addWidget(checkbox)

        # Add category checkboxes to the category filter area:
        for category in sorted(CATEGORY):
            checkbox = QCheckBox(category)
            self.filter_checkboxes.append(checkbox)
            checkbox.setChecked(True)
            checkbox.stateChanged.connect(lambda checked, value=category.lower():
                                          self.filter_tree_items(attribute='category',
                                                                 value=value,
                                                                 check_state=checked))
            self.categoryFiltersLayout.addWidget(checkbox)

        # Add status checkboxes to the status filters area:
        self.db_api.filters = {'applies_to': 'All'}
        for status in self.db_api.get_entities(entity='Status', filtered=True):
            checkbox = QCheckBox(status.label)
            self.filter_checkboxes.append(checkbox)
            checkbox.setChecked(True)
            checkbox.stateChanged.connect(lambda checked, value=status.id:
                                          self.filter_tree_items(attribute='status_id',
                                                                 value=value,
                                                                 check_state=checked))
            self.statusFiltersLayout.addWidget(checkbox)

        # Set the user filter to the current user:
        self.userFilterLineEdit.setText(get_current_user())

        # Set up the asset tree view model:
        self.item_model = QStandardItemModel()
        self.assetTreeView.setModel(self.item_model)
        self.refresh_tree_data()

    def refresh_tree_data(self):
        """
        Setup the asset tree data.
        :return: None;
        """
        self.item_model.clear()
        self.clear_details_layout()
        # Disable all signals while altering the asset tree:
        for checkbox in self.filter_checkboxes:
            checkbox.blockSignals(True)
            checkbox.setChecked(True)
            checkbox.blockSignals(False)
        # Set up the default header and the default column sizes:
        self.item_model.setHorizontalHeaderLabels(['Entity', 'Type', 'Created by', 'Status'])
        self.assetTreeView.header().resizeSection(0, 250)
        self.assetTreeView.header().resizeSection(1, 80)
        self.assetTreeView.header().resizeSection(2, 100)
        self.assetTreeView.header().resizeSection(3, 80)
        self.build_tree_data()
        # Show all of the fields in the tree:
        self.assetTreeView.expandAll()

    def clear_details_layout(self):
        """
        Remove all data from the selection details area.
        :return: None;
        """
        for i in range(self.selectionDetailsLayout.count()):
            self.selectionDetailsLayout.itemAt(i).widget().deleteLater()

    def slot_item_clicked(self, index):
        """
        Commands to execute when a tree item has been clicked.
        :param index: the index of the item clicked;
        :return: None;
        """
        self.clear_details_layout()
        # Recreate the item's unique ID:
        item_id = '{0}{1}'.format(index.internalId(), index.row())

        if item_id in self.item_entity_dict:
            # Find the item's corresponding entity and display all of its attributes and their respective values:
            entity = self.item_entity_dict[item_id]['entity']
            entity_data = entity.jsonify()
            for key, value in sorted(entity_data.items()):
                if value and type(value) in [str, int, float, datetime]:
                    label = QLabel('{0}: {1}'.format(key, value))
                    label.setTextInteractionFlags(Qt.TextSelectableByMouse)
                    self.selectionDetailsLayout.addWidget(label)
            
            if entity.type == 'Version':
                label = QLabel('files: {}'.format([file.full_name for file in entity.files]))
                label.setTextInteractionFlags(Qt.TextSelectableByMouse)
                self.selectionDetailsLayout.addWidget(label)

                show_dependency_btn = QPushButton('Show Dependencies')
                self.selectionDetailsLayout.addWidget(show_dependency_btn)
                show_dependency_btn.clicked.connect(lambda: self.show_version_dependencies(version=entity,
                                                                                           button=show_dependency_btn))

    def show_version_dependencies(self, version, button):
        """
        A slot method for creating a tree widget to display a given version's dependencies.
        :param version: a version entity;
        :param button: a QPushButton;
        :return: None;
        """
        tree_widget = QTreeWidget()
        tree_widget.setHeaderItem(QTreeWidgetItem(['Asset', 'Resource', 'Version', 'Action']))
        self.selectionDetailsLayout.addWidget(tree_widget)
        for dependency in version.dependencies:
            self.append_to_dependency_tree(parent_item=tree_widget, entity=dependency)
        button.setHidden(True)

    def append_to_dependency_tree(self, parent_item, entity):
        """
        Add a data row to the data tree and store its main item under a unique ID for future reference.
        :param parent_item: the parent item of the item to be created;
        :param entity: the entity to be represented;
        :return: a newly created data tree item;
        """

        item = QTreeWidgetItem(parent_item)
        find_btn = QPushButton('Find', parent_item)
        find_btn.clicked.connect(lambda: self.find_version_item(version=entity))

        parent_item.setItemWidget(item, 0, QLabel(entity.resource.entity.label, parent_item))
        parent_item.setItemWidget(item, 1, QLabel(entity.resource.label, parent_item))
        parent_item.setItemWidget(item, 2, QLabel(entity.label, parent_item))
        parent_item.setItemWidget(item, 3, find_btn)

        return item

    def find_version_item(self, version):
        """
        Find an item associated with a given version entity.
        :param version: a version entity;
        :return: None;
        """
        item = self.version_item_dict[version.uuid]
        self.assetTreeView.setCurrentIndex(item.index())
        self.slot_item_clicked(index=item.index())

    def slot_item_double_clicked(self, index):
        """
        Commands to be executed when a tree item has been double clicked.
        :param index: the index of the double-clicked item;
        :return: None;
        """
        # Recreate the item's unique ID:
        item_id = '{0}{1}'.format(index.internalId(), index.row())
        if item_id in self.item_entity_dict:
            # Find the item's corresponding entity, determine its path on the disk and locate it in the file explorer:
            entity = self.item_entity_dict[item_id]['entity']
            path = self.db_api.get_vfx_path(path_type='publish', entity=entity)

            if os.path.exists(path):
                browse_path(path)
            else:
                msg = 'Could not find VFX publish path %s for %s %s, unique ID: %s' % (path,
                                                                                       entity.type.lower(),
                                                                                       entity.name,
                                                                                       entity.uuid)
                error_message(self, msg)

    def build_tree_data(self):
        """
        Represent the database as items in the UI's data tree.
        :return: None;
        """
        start = datetime.now()  # To be used for timing the building process duration.
        selected_project = self.projectComboBox.currentText()
        if not selected_project:
            return

        self.item_entity_dict = {}
        # Create an invisible root item for the data tree:
        data_tree_root = self.item_model.invisibleRootItem()

        # Create the project item:
        project_entity = self.project_dict[selected_project]
        project_item = self.append_data_tree_row(parent_item=data_tree_root, entity=project_entity)

        # Create tree branches to separate assets from sequences:
        assets_item = QStandardItem('Assets')
        sequences_item = QStandardItem('Sequences')
        project_item.appendRow([sequences_item, QStandardItem(''), QStandardItem(''), QStandardItem('')])
        project_item.appendRow([assets_item, QStandardItem(''), QStandardItem(''), QStandardItem('')])

        # Create asset/resource/version items:
        for asset_entity in project_entity.assets:
            asset_item = self.append_data_tree_row(parent_item=assets_item, entity=asset_entity)

            for resource_entity in asset_entity.resources:
                resource_item = self.append_data_tree_row(parent_item=asset_item, entity=resource_entity)
                for version_entity in resource_entity.versions:
                    self.append_data_tree_row(parent_item=resource_item, entity=version_entity)

        # Create sequence/shot/resource/version items:
        for sequence_entity in project_entity.sequences:
            sequence_item = self.append_data_tree_row(parent_item=sequences_item, entity=sequence_entity)
            for shot_entity in sequence_entity.shots:
                shot_item = self.append_data_tree_row(parent_item=sequence_item, entity=shot_entity)
                for resource_entity in shot_entity.resources:
                    resource_item = self.append_data_tree_row(parent_item=shot_item, entity=resource_entity)
                    for version_entity in resource_entity.versions:
                        self.append_data_tree_row(parent_item=resource_item, entity=version_entity)

        end = datetime.now()  # To mark the end of the build process.
        self.logger.debug('Browser data tree for project %s built in %s seconds.' % (selected_project, end-start))

    def append_data_tree_row(self, parent_item, entity):
        """
        Add a data row to the data tree and store its main item under a unique ID for future reference.
        :param parent_item: the parent item of the item to be created;
        :param entity: the entity to be represented;
        :return: a newly created data tree item;
        """
        item = QStandardItem(entity.label)
        parent_item.appendRow([item,
                              QStandardItem(entity.type.lower()),
                              QStandardItem(entity.created_by),
                              QStandardItem(entity.status.label)])
        index = item.index()
        # Create a unique index ID based on the index internal ID and row number:
        index_id = '{0}{1}'.format(index.internalId(), index.row())
        # Store the item-to-entity relationship for future reference:
        self.item_entity_dict[index_id] = dict()
        self.item_entity_dict[index_id]['item'] = item
        self.item_entity_dict[index_id]['entity'] = entity
        self.item_entity_dict[index_id]['parent'] = parent_item
        self.item_entity_dict[index_id]['hidden'] = False

        if entity.type == 'Version':
            self.version_item_dict[entity.uuid] = item
        return item

    def set_tree_item_visibility(self, item, hidden, silent=False):
        """
        Show/hide an item in the data tree.
        :param item: the item to show/hide;
        :param hidden: a boolean to determine if the item should be hidden or not;
        :param silent: a boolean to determine if the hidden state should be reflected in the item-entity dictionary;
        :return: None;
        """
        item_id = '{0}{1}'.format(item.index().internalId(), item.index().row())
        parent_item = self.item_entity_dict[item_id]['parent']
        self.assetTreeView.setRowHidden(item.row(), parent_item.index(), hidden)
        if not silent:
            self.item_entity_dict[item_id]['hidden'] = hidden

    def filter_tree_items(self, attribute, value, check_state):
        """
        Show/hide certain data tree items based on the attribute-value pair of their respective entities.
        :param attribute: the entity's attribute name (string);
        :param value: the value of the entity's attribute (string);
        :param check_state: the check state, from which to determine the hidden state;
        :return: None;
        """
        if check_state:
            hidden = False
        else:
            hidden = True

        for item_id in self.item_entity_dict:
            item = self.item_entity_dict[item_id]['item']
            entity = self.item_entity_dict[item_id]['entity']

            # Ignore projects as they should be visible at all times:
            if entity.type is 'Project':
                continue

            entity_attributes = entity.jsonify()

            if attribute in entity_attributes:
                if entity_attributes[attribute] == value:
                    self.set_tree_item_visibility(item=item, hidden=hidden)

    def filter_by_user(self):
        """
        Filter all data tree items by user.
        :return: None;
        """
        hidden = not self.userFilterCheckBox.isChecked()
        user = self.userFilterLineEdit.text().lower()
        if hidden:
            for item_id in self.item_entity_dict:
                item = self.item_entity_dict[item_id]['item']
                parent_item = self.item_entity_dict[item_id]['parent']
                hidden = self.item_entity_dict[item_id]['hidden']
                self.assetTreeView.setRowHidden(item.row(), parent_item.index(), hidden)
        else:
            for item_id in self.item_entity_dict:
                item = self.item_entity_dict[item_id]['item']
                entity = self.item_entity_dict[item_id]['entity']
                item_hidden = self.item_entity_dict[item_id]['hidden']
                # Ignore projects as they should be visible at all times:
                if entity.type is 'Project':
                    continue
                if entity.created_by == user and not item_hidden:
                    self.set_tree_item_visibility(item=item, hidden=False, silent=True)
                else:
                    self.set_tree_item_visibility(item=item, hidden=True, silent=True)

    def user_filter_change_cmd(self):
        """
        Apply the user filter if the user filter check box is enabled.
        :return: None;
        """
        if self.userFilterCheckBox.isChecked():
            self.filter_by_user()

    def find_item(self, omit=None):
        """
        Select the data tree item that most closely matches the find field input.
        :param omit: an item to omit from the selection process;
        :return: None;
        """
        identifier = self.findLineEdit.text().lower()  # The find field input.
        match_ratio = 0.0  # To contain the percentage of similarity between a given item and the find field input.
        for item_id in self.item_entity_dict:
            item = self.item_entity_dict[item_id]['item']
            if item is not omit:
                # Determine the similarity between the two strings:
                ratio = SequenceMatcher(None, identifier, item.text()).ratio()
                # If there is a higher similarity ratio, select the item and update the current match ratio:
                if ratio > match_ratio:
                    match_ratio = ratio
                    self.assetTreeView.setCurrentIndex(item.index())
                    self.slot_item_clicked(item.index())

    def find_next_cmd(self):
        """
        Find the next most similar item in the data tree based on the find field input.
        :return: None;
        """
        selection = self.assetTreeView.selectedIndexes()
        if selection:
            index = selection[0]
            item = index.model().itemFromIndex(index)
            self.find_item(omit=item)

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.projectComboBox.currentIndexChanged.connect(self.refresh_tree_data)
        self.assetTreeView.clicked.connect(self.slot_item_clicked)
        self.assetTreeView.doubleClicked.connect(self.slot_item_double_clicked)
        self.findLineEdit.textChanged.connect(self.find_item)
        self.findNextBtn.clicked.connect(self.find_next_cmd)
        self.userFilterCheckBox.stateChanged.connect(self.filter_by_user)
        self.userFilterLineEdit.textChanged.connect(self.user_filter_change_cmd)
        self.refreshBtn.clicked.connect(self.refresh_tree_data)
        self.closeBtn.clicked.connect(self.close)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show():
    """
    Show the UI.
    :return: None;
    """
    browser = Browser()
    browser.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'browser.png')))
    browser.setWindowTitle('Piper Browser {}'.format(__version__))
    browser.show()


if __name__ == '__main__':
    run()
