# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
from piper.config import ICONS_PATH, STUDIO_CONFIG, STUDIO_CONFIG_FILE
from piper.gui.qt import QMainWindow, load_qt_ui, \
    QIcon, QTableWidgetItem, QStandardItemModel, QStandardItem, QAbstractItemView, QApplication
from piper.gui.widgets.create_entity import CreateEntity
from piper.gui.widgets.add_software import AddSoftware
from piper.gui.utils import info_message, error_message, confirm_dialog
from piper.api.database_api import DbApi
from piper.api.utils import write_json, read_json
from piper.api.logger import get_logger

# Table header constants:
ARTISTS_TABLE_HEADER = ['Name', 'Email', 'Department', 'Status', 'Is Administrator', 'Description', 'Comment']
DEPARTMENT_TABLE_HEADER = ['Name', 'Email', 'Status', 'Description', 'Comment']
GLOBALS_HEADER = ['Attribute', 'Value']


class StudioConfig(QMainWindow):
    """
    An class to manage modifying studio configurations.
    """
    def __init__(self):
        super(StudioConfig, self).__init__()
        load_qt_ui('studio_config.ui', self)
        self.logger = get_logger('Piper Studio Config')
        self.db_api = DbApi()
        self.user = self.db_api.get_current_artist()
        if self.user.is_administrator:
            self.studio_config = STUDIO_CONFIG.data.copy()
            self.item_model = None
            self._data = {
                'artists': {},
                'departments': {},
                'items': {}
            }
            self.selected_item = None
            self._setup_data()
            self._connect_widget_cmds()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """
        self.artistsTableWidget.setColumnCount(len(ARTISTS_TABLE_HEADER))
        self.artistsTableWidget.setHorizontalHeaderLabels(ARTISTS_TABLE_HEADER)
        self.artistsTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.populate_table(table_widget=self.artistsTableWidget, entity_type='Artist')

        self.departmentsTableWidget.setColumnCount(len(DEPARTMENT_TABLE_HEADER))
        self.departmentsTableWidget.setHorizontalHeaderLabels(DEPARTMENT_TABLE_HEADER)
        self.departmentsTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.populate_table(table_widget=self.departmentsTableWidget, entity_type='Department')

        self.item_model = QStandardItemModel()
        self.item_model.itemChanged.connect(self.config_value_updated)

        self.globalsTreeView.setModel(self.item_model)
        self.refresh_tree_data()

    def populate_table(self, table_widget, entity_type):
        """
        Populate a given table widget with a provided entity type.
        :param table_widget: the table widget to populate;
        :param entity_type: the (str) entity type;
        :return: None;
        """
        table_widget.clearContents()
        entities = self.db_api.get_entities(entity=entity_type)
        table_widget.setRowCount(len(entities))
        for i, entity in enumerate(reversed(entities)):
            self._data['{}s'.format(entity.type.lower())][i] = entity
            self._add_to_table_row(row_index=i, entity=entity)
        table_widget.resizeColumnsToContents()

    def _add_to_table_row(self, row_index, entity):
        """
        Add a table row representation of a given entity.
        :param row_index: the (int) row index of the table;
        :param entity: a given entity;
        :return: None;
        """
        if entity.type == 'Artist':
            self.artistsTableWidget.setItem(row_index, 0, QTableWidgetItem(entity.label))
            self.artistsTableWidget.setItem(row_index, 1, QTableWidgetItem(entity.email))
            self.artistsTableWidget.setItem(row_index, 2, QTableWidgetItem(entity.department.label))
            self.artistsTableWidget.setItem(row_index, 3, QTableWidgetItem(entity.status.label))
            self.artistsTableWidget.setItem(row_index, 4, QTableWidgetItem(str(entity.is_administrator)))
            self.artistsTableWidget.setItem(row_index, 5, QTableWidgetItem(entity.description))
            self.artistsTableWidget.setItem(row_index, 6, QTableWidgetItem(entity.comment))
        else:
            self.departmentsTableWidget.setItem(row_index, 0, QTableWidgetItem(entity.label))
            self.departmentsTableWidget.setItem(row_index, 1, QTableWidgetItem(entity.email))
            self.departmentsTableWidget.setItem(row_index, 2, QTableWidgetItem(entity.status.label))
            self.departmentsTableWidget.setItem(row_index, 3, QTableWidgetItem(entity.description))
            self.departmentsTableWidget.setItem(row_index, 4, QTableWidgetItem(entity.comment))

    def refresh_tree_data(self):
        """
        Clear and rebuild the entire configuration data tree.
        :return: None;
        """
        self.item_model.clear()
        self.item_model.setHorizontalHeaderLabels(GLOBALS_HEADER)
        self.build_tree_data(root_item=self.item_model.invisibleRootItem(), data_dict=self.studio_config)
        self.globalsTreeView.header().resizeSection(0, 200)
        self.globalsTreeView.expandAll()
        self.selected_item = None

    def build_tree_data(self, root_item, data_dict):
        """
        Build up the tree rows that represent that data in a given data dictionary, starting from a given root Qt item.
        :param root_item: the top level Qt item;
        :param data_dict: the configuration data dictionary;
        :return: None;
        """
        for key, value in data_dict.items():
            if type(value) == dict:
                item = self.append_data_tree_row(parent_item=root_item, key=key)
                self.build_tree_data(root_item=item, data_dict=data_dict[key])
            else:
                self.append_data_tree_row(parent_item=root_item, key=key, value=str(value))

    def append_data_tree_row(self, parent_item, key, value=''):
        """
        Add a new row to a given data tree parent item.
        :param parent_item: a Qt parent item;
        :param key: the (str) key to be displayed;
        :param value: the (str) value to be displayed;
        :return: a newly created Qt standard item;
        """
        key_item = QStandardItem(key)
        value_item = QStandardItem(value)

        key_item.setEditable(False)
        if not value:
            value_item.setEditable(False)

        parent_item.appendRow([key_item, value_item])

        self._data['items'][self._item_uuid(value_item)] = key_item

        return key_item

    @staticmethod
    def _item_uuid(item):
        """
        Generate a unique ID for a given Qt item.
        :param item: a Qt item;
        :return: a (str) unique identifier;
        """
        return '{0}{1}'.format(item.index().internalId(), item.row())

    def update_studio_config(self):
        """
        Update the studio configuration file.
        :return: a Qt info message if successful, otherwise - None;
        """
        if confirm_dialog(parent=self, question='Are you sure you want to overwrite the studio config file?'):
            studio_config = read_json(STUDIO_CONFIG_FILE)
            studio_config[sys.platform] = self.studio_config
            write_json(dict=studio_config, path=STUDIO_CONFIG_FILE)
            STUDIO_CONFIG.reload()
            return info_message(parent=self, msg='Studio config file has been updated!')
        return None

    def config_value_updated(self, item):
        """
        A slot for storing configuration value changes.
        :param item: the Qt item that has been changed;
        :return: None;
        """
        key = self._data['items'][self._item_uuid(item)].text()
        if item.parent().parent():
            self.studio_config[item.parent().parent().text()][item.parent().text()][key] = item.text()
        else:
            self.studio_config[item.parent().text()][key] = item.text()

    def crate_entity(self, entity_type, entity=None, edit_mode=False):
        """
        Run the entity creation dialog.
        :param entity_type: the (str) type of the entity;
        :param entity: the entity;
        :param edit_mode: a boolean to signify whether or not the dialog should be in edit mode;
        :return: None;
        """
        create_entity_dialog = CreateEntity(parent=self, entity_type=entity_type, entity=entity, edit_mode=edit_mode)
        create_entity_dialog.exec_()
        if self.studioTabWidget.currentIndex():
            self.populate_table(table_widget=self.departmentsTableWidget, entity_type='Department')
        else:
            self.populate_table(table_widget=self.artistsTableWidget, entity_type='Artist')

    def update_entity(self, index):
        """
        Update the entity represented by a given widget cell.
        :param index: the (str) index of the double clicked widget cell;
        :return: None;
        """
        if self.studioTabWidget.currentIndex():
            entity = self._data['departments'][index]
        else:
            entity = self._data['artists'][index]

        self.crate_entity(entity_type=entity.type, entity=entity, edit_mode=True)

    def add_software(self):
        create_entity_dialog = AddSoftware(parent=self)
        create_entity_dialog.exec_()
        self.studio_config = STUDIO_CONFIG.data.copy()
        self.refresh_tree_data()

    def update_selected_item(self, index):
        self.selected_item = self._data['items']['{0}{1}'.format(index.internalId(), index.row())]

    def remove_software(self):
        if not self.selected_item:
            return error_message(parent=self, msg='No software selected.')
        selected_item_parent = self.selected_item.parent()
        if not selected_item_parent or selected_item_parent.text() != 'SOFTWARE':
            return error_message(parent=self, msg='Selection is not software.')
        software = self.selected_item.text()
        if confirm_dialog(parent=self,
                          question='Are you sure you want to remove {}\
                          from the studio configuration for ALL platforms?'.format(software)):

            self.studio_config['SOFTWARE'].pop(software, None)
            if self.update_studio_config():
                self.refresh_tree_data()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.applyBtn.clicked.connect(self.update_studio_config)
        self.addArtistBtn.clicked.connect(lambda: self.crate_entity(entity_type='Artist'))
        self.addDepartmentBtn.clicked.connect(lambda: self.crate_entity(entity_type='Department'))
        self.artistsTableWidget.cellDoubleClicked.connect(self.update_entity)
        self.departmentsTableWidget.cellDoubleClicked.connect(self.update_entity)
        self.addSoftwareBtn.clicked.connect(self.add_software)
        self.globalsTreeView.clicked.connect(self.update_selected_item)
        self.removeSoftwareBtn.clicked.connect(self.remove_software)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show():
    """
    Show the UI.
    :return: None;
    """
    parent_window = QApplication.activeWindow()
    studio_config = StudioConfig()
    if not studio_config.user.is_administrator:
        error_message(parent=parent_window,
                      msg='You do not have the administrative permissions to use Studio Config, sorry!')
        studio_config.deleteLater()
    studio_config.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'studio_config.png')))
    studio_config.setWindowTitle('Piper Studio Config {}'.format(__version__))
    studio_config.show()


if __name__ == '__main__':
    run()
