# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
from piper.config import ICONS_PATH
from piper.gui.qt import QMainWindow, QApplication, QListWidgetItem, QIcon, load_qt_ui
from piper.gui.utils import warning_message, info_message, error_message, confirm_dialog, set_combo_box_text
from piper.gui.widgets.cascading_config import CascadingConfig
from piper.api.database_api import DbApi
from piper.api.logger import get_logger


class ProjectManager(QMainWindow):
    """
    An app to create and configure project data.
    """
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        load_qt_ui('project_manager.ui', self)
        self.logger = get_logger('Piper Project Manager')
        self._data = {
            'projects': {},
            'sequences': {},
            'shots': {},
            'assets': {},
            'asset_types': {}
        }  # To store label-to-entity references.
        self.selected_project = None
        self.db_api = DbApi()
        self.user = self.db_api.get_current_artist()
        if self.user.is_administrator:
            self._setup_data()
            self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.refresh_project_combo_box()
        self.selected_project = self.projectComboBox.currentText()
        asset_types = self.db_api.get_entities(entity='AssetType', filtered=True)

        for asset_type in asset_types:
            self._data['asset_types'][asset_type.label] = asset_type
            self.assetTypeComboBox.addItem(asset_type.label)

        self.project_combo_box_change_cmd()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.createProjectBtn.clicked.connect(self.create_project_cmd)
        self.removeProjectBtn.clicked.connect(self.remove_project_cmd)
        self.createAssetBtn.clicked.connect(self.create_asset_cmd)
        self.removeAssetBtn.clicked.connect(self.remove_asset_cmd)
        self.createSequenceBtn.clicked.connect(self.create_sequence_cmd)
        self.removeSequenceBtn.clicked.connect(self.remove_sequence_cmd)
        self.createShotBtn.clicked.connect(self.create_shot_cmd)
        self.removeShotBtn.clicked.connect(self.remove_shot_cmd)
        self.projectComboBox.currentIndexChanged.connect(self.project_combo_box_change_cmd)
        self.projectConfigBtn.clicked.connect(self.project_config_cmd)
        self.assetConfigBtn.clicked.connect(self.asset_config_cmd)
        self.sequenceConfigBtn.clicked.connect(self.sequence_config_cmd)
        self.shotConfigBtn.clicked.connect(self.shot_config_cmd)
        self.sequenceListWidget.currentItemChanged.connect(lambda: self.refresh_shot_list_widget())
        self.assetTypeComboBox.currentIndexChanged.connect(lambda: self.refresh_asset_list_widget())
        self.closeBtn.clicked.connect(self.close)

    def create_project_cmd(self):
        """
        Create a new project if it doesn't already exist.
        :return: None;
        """
        name = self.newProjectLineEdit.text().lower()

        if not name:
            warning_message(self, 'The project name field is empty!\nPlease specify a project name.')
            return

        if self.db_api.create_entity(entity='Project', name=name):
            info_message(self, 'Project {0} successfully created!'.format(name))
            self.refresh_project_combo_box(set_selected=name)
            self.newProjectLineEdit.clear()
        else:
            error_message(self, 'Project {0} already exists!'.format(name))

    def remove_project_cmd(self):
        """
        Remove the selected project.
        :return: None;
        """
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')
        if confirm_dialog(self, 'Are you sure you want to remove project {0}?'.format(self.selected_project)):
            project_entity = self._data['projects'][self.selected_project]
            self.db_api.remove_entity(project_entity)
            self.logger.info('Project %s has been removed.' % self.selected_project)
            self.refresh_project_combo_box()

    def create_asset_cmd(self):
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')

        asset_name = self.assetLineEdit.text()

        if not asset_name:
            warning_message(self, 'The asset name field is empty!\nPlease specify an asset name.')
            return

        asset_type = self._data['asset_types'][self.assetTypeComboBox.currentText()]
        asset_name = '{0}_{1}'.format(asset_type.name, asset_name)
        project = self._data['projects'][self.selected_project]
        if self.db_api.create_entity(entity='Asset',
                                     name=asset_name,
                                     project=project,
                                     asset_type=asset_type):
            self.refresh_asset_list_widget(set_selected=asset_name)
            self.assetLineEdit.clear()
        else:
            error_message(self, 'Asset {0} of project {1} already exists!'.format(asset_name, self.selected_project))

    def remove_asset_cmd(self):
        asset_removed = False
        for asset in self.assetListWidget.selectedItems():
            asset_name = asset.text()

            if confirm_dialog(self, 'Are you sure you want to remove asset {0}?'.format(asset_name)):
                asset_entity = self._data['assets'][asset_name]
                self.db_api.remove_entity(asset_entity)
                self.logger.info('Asset %s has been removed.' % asset_name)
                asset_removed = True
        if asset_removed:
            self.refresh_asset_list_widget()

    def create_sequence_cmd(self):
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')

        sequence_name = self.sequenceLineEdit.text()

        if not sequence_name:
            return warning_message(self, 'The sequence name field is empty!\nPlease specify a sequence name.')

        project = self._data['projects'][self.selected_project]
        if self.db_api.create_entity(entity='Sequence',
                                     name=sequence_name,
                                     project=self._data['projects'][self.selected_project]):
            self.refresh_sequence_list_widget(set_selected=sequence_name)
            self.sequenceLineEdit.clear()
        else:
            error_message(self, 'Sequence {0} of project {1} already exists!'.format(sequence_name, 
                                                                                     self.selected_project))

    def remove_sequence_cmd(self):
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')
        sequence_removed = False

        for sequence in self.sequenceListWidget.selectedItems():
            sequence_name = sequence.text()
            if confirm_dialog(self, 'Are you sure you want to remove sequence {0}?'.format(sequence_name)):
                sequence_entity = self._data['sequences'][sequence_name]
                self.db_api.remove_entity(sequence_entity)
                self.logger.info('Sequence %s has been removed.' % sequence_name)
                sequence_removed = True
        if sequence_removed:
            self.refresh_sequence_list_widget()
            self.refresh_shot_list_widget()

    def create_shot_cmd(self):
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')

        shot_name = self.shotLineEdit.text()

        if not shot_name:
            return warning_message(self, 'The shot name field is empty!\nPlease specify a shot name.')

        selected_sequence = self.sequenceListWidget.currentItem()
        if not selected_sequence:
            return warning_message(self, 'No sequence has been selected!\nPlease select a sequence first.')

        sequence = self._data['sequences'][selected_sequence.text()]

        if self.db_api.create_entity(entity='Shot',
                                     name=shot_name,
                                     project=self._data['projects'][self.selected_project],
                                     sequence=sequence):
            self.refresh_shot_list_widget(set_selected=shot_name)
            self.shotLineEdit.clear()
        else:
            error_message(self, 'Shot {0} of sequence {1} already exists!'.format(shot_name, sequence.name))

    def remove_shot_cmd(self):
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')

        shot_removed = False

        selected_sequence = self.sequenceListWidget.currentItem()
        if selected_sequence:
            for shot in self.shotListWidget.selectedItems():
                shot_name = shot.text()
                if confirm_dialog(self, 'Are you sure you want to remove shot {0}?'.format(shot_name)):
                    shot_entity = self._data['shots'][shot_name]
                    self.db_api.remove_entity(shot_entity)
                    self.logger.info('Shot %s has been removed.' % shot_name)
                    shot_removed = True
            if shot_removed:
                self.refresh_shot_list_widget()

    def refresh_project_combo_box(self, set_selected=None):
        self.projectComboBox.clear()
        for project in self.db_api.get_entities(entity='Project', filtered=True):
            self._data['projects'][project.label] = project
            self.projectComboBox.addItem(project.label)
        if set_selected:
            set_combo_box_text(combo_box=self.projectComboBox, text=set_selected)

    def refresh_asset_list_widget(self, set_selected=None):
        self.assetListWidget.clear()
        self.db_api.filters['project'] = self._data['projects'][self.selected_project]
        assets = self.db_api.get_entities(entity='Asset', filtered=True)
        for asset in assets:
            if asset.asset_type == self._data['asset_types'][self.assetTypeComboBox.currentText()]:
                self._data['assets'][asset.label] = asset
                item = QListWidgetItem(asset.label)
                self.assetListWidget.addItem(item)
            if set_selected:
                self.assetListWidget.setCurrentRow([a.label for a in assets].index(set_selected))

    def refresh_sequence_list_widget(self, set_selected=None):
        self.sequenceListWidget.clear()
        self.db_api.filters['project'] = self._data['projects'][self.selected_project]
        sequences = self.db_api.get_entities(entity='Sequence', filtered=True)
        for sequence in sequences:
            self._data['sequences'][sequence.label] = sequence
            item = QListWidgetItem(sequence.label)
            self.sequenceListWidget.addItem(item)
        if set_selected:
            self.sequenceListWidget.setCurrentRow([s.label for s in sequences].index(set_selected))

    def refresh_shot_list_widget(self, set_selected=None):
        self.shotListWidget.clear()
        selected_sequence = self.sequenceListWidget.currentItem()
        if selected_sequence:
            self.db_api.filters['sequence'] = self._data['sequences'][selected_sequence.text()]
            shots = self.db_api.get_entities(entity='Shot', filtered=True)
            for shot in shots:
                self._data['shots'][shot.label] = shot
                item = QListWidgetItem(shot.label)
                self.shotListWidget.addItem(item)
            if set_selected:
                self.shotListWidget.setCurrentRow([s.label for s in shots].index(set_selected))

    def project_config_cmd(self):
        if not self.selected_project:
            return warning_message(self, 'No project has been selected!\nPlease select a project first.')
        self.run_cascading_config(entity=self._data['projects'][self.selected_project])

    def asset_config_cmd(self):
        selected_asset = self.assetListWidget.currentItem()
        if selected_asset:
            asset_entity = self._data['assets'][selected_asset.text()]
            self.run_cascading_config(entity=asset_entity)

    def sequence_config_cmd(self):
        selected_sequence = self.sequenceListWidget.currentItem()
        if selected_sequence:
            sequence_entity = self._data['sequences'][selected_sequence.text()]
            self.run_cascading_config(entity=sequence_entity)

    def shot_config_cmd(self):
        selected_shot = self.shotListWidget.currentItem()
        selected_sequence = self.sequenceListWidget.currentItem()
        if selected_shot and selected_sequence:
            shot_entity = self._data['shots'][selected_shot.text()]
            self.run_cascading_config(entity=shot_entity)

    def run_cascading_config(self, entity):
        cascading_config_dialog = CascadingConfig(parent=self,
                                                  entity=entity)
        cascading_config_dialog.exec_()

    def project_combo_box_change_cmd(self):
        self.selected_project = self.projectComboBox.currentText()
        if self.selected_project:
            self.refresh_asset_list_widget()
            self.refresh_sequence_list_widget()
            self.refresh_shot_list_widget()


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show():
    """
    Show the UI.
    :return: None;
    """
    parent_window = QApplication.activeWindow()
    project_manager = ProjectManager()
    if not project_manager.user.is_administrator:
        error_message(parent=parent_window,
                      msg='You do not have the administrative permissions to use Project Manager, sorry!')
        project_manager.deleteLater()
    project_manager.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'project_manager.png')))
    project_manager.setWindowTitle('Piper Project Manager {}'.format(__version__))
    project_manager.show()


if __name__ == '__main__':
    run()
