# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
from glob import glob
from shutil import copyfile
from piper.config import ICONS_PATH, STUDIO_CONFIG
from piper.gui.qt import QIcon, QMainWindow, QApplication, QLabel, load_qt_ui
from piper.gui.utils import warning_message, error_message, info_message, set_combo_box_text
from piper.gui.widgets.file_selector import FileSelector
from piper.api.database_api import DbApi
from piper.api.logger import get_logger
from piper.api.dcc import load_dcc_api
from piper.api.utils import environment_check


class Publisher(QMainWindow):
    """
    An app for publishing data to the database.
    """
    def __init__(self, dcc=None, ui=True):
        super(Publisher, self).__init__()
        self.db_api = DbApi()
        self.dcc_api = None
        if dcc:
            self.dcc_api = load_dcc_api(dcc.lower())
        else:
            dcc = 'standalone'
        self.logger = get_logger('Piper Publisher')
        self.logger.info('Starting in %s...' % dcc)
        self.file_selector = None

        self.current_task = self.db_api.get_entity(entity='Task', uuid=os.environ['PIPER_TASK_UUID'])

        # TODO: Specify publish method for each pipeline step:
        publish_cmd_dict = {'cmp': self.general_publish,
                            'slp': self.general_publish,
                            'pnt': self.general_publish,
                            'rot': self.general_publish,
                            'mod': self.general_publish,
                            'ani': self.general_publish,
                            'rig': self.general_publish,
                            'cam': self.general_publish,
                            'lgt': self.general_publish,
                            'srf': self.general_publish,
                            'ldv': self.general_publish,
                            'efx': self.general_publish,
                            'cfx': self.general_publish,
                            'lay': self.general_publish,
                            'mmv': self.general_publish,
                            'env': self.general_publish,
                            'cpt': self.general_publish,
                            'plt': self.general_publish,
                            'dmp': self.general_publish,
                            'txr': self.general_publish}

        self._publish_cmd = publish_cmd_dict[self.current_task.pipeline_step.name]

        if not environment_check('PIPER_RESOURCE_UUID'):
            self.logger.error('Could not find valid resource, aborting.')
            error_message(parent=self, msg='Valid resource not found!\
                          \nPlease open a resource version or snapshot from the Piper Scene Builder.')
        else:
            self.selected_resource = self.db_api.get_entity(entity='Resource',
                                                            uuid=os.environ['PIPER_RESOURCE_UUID'])

        if ui:
            load_qt_ui('publisher.ui', self)
            self.mainLabel.setText('Publish {0}'.format(self.selected_resource.label))
            self._setup_data()
            self._connect_widget_cmds()
            self.show()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None
        """
        self.file_selector = FileSelector(parent=self)
        current_file = self.dcc_api.get_current_file_name()
        if not current_file:
            current_file = 'current work file'
        self.publishingLabel.setText('Source file: {}'.format(current_file))
        self.filesLayout.addWidget(self.file_selector)
        self.file_selector.selectFilesGrp.setTitle('Additional file(s):')
        self.projectLabel.setText('Project: {}'.format(self.selected_resource.project.label))
        self.entityLabel.setText('Entity: {0}_{1}'.format(self.selected_resource.task.entity.sequence.label,
                                                          self.selected_resource.task.entity.label))
        self.resourceLabel.setText('Resource: {}'.format(self.selected_resource.label))

    def _get_arg_dict(self):
        """
        Gather publishing data from the UI and calls the relevant publishing functions
        based on what is to be published.
        :return: None;
        """
        description = self.descriptionTextEdit.toPlainText()
        comment = self.commentLineEdit.text()

        if not (description or comment):
            warning_message(parent=self, msg='Please provide a description or leave a comment before publishing!')
            return None

        arg_dict = {'description': description,
                    'comment': comment,
                    'file_format': self.dcc_api.get_current_file_format()}
        return arg_dict

    def publish(self, arg_dict=None):
        """
        Run the appropirate publishing method with relevant arguments.
        :param arg_dict: publishing arguments dictionary if applicable;
        :return: None
        """
        if not arg_dict:
            arg_dict = self._get_arg_dict()

        if arg_dict:
            self._publish_cmd(arg_dict=arg_dict)

    def general_publish(self, arg_dict):
        """
        A general publishing method.
        :param arg_dict: publishing arguments dictionary if applicable;
        :return: None;
        """
        version, published_file = self.publish_version(resource=self.selected_resource, arg_dict=arg_dict)
        self.dcc_api.export_to_published_file(file_entity=published_file,
                                              published_file_path=self.db_api.get_vfx_path(path_type='publish',
                                                                                           entity=published_file))

    def publish_version(self, resource, arg_dict):
        """
        Publish a new version and related publish files.
        :param resource: the resource entity to create a new version of;
        :param arg_dict: publishing arguments dictionary;
        :return: version entity, primary published file entity;
        """
        # Create a version of the given resource:
        version = self.db_api.create_entity(entity='Version',
                                            name=self.db_api.get_new_version_name(resource=resource),
                                            resource=resource,
                                            project=resource.project,
                                            artist=self.db_api.get_current_artist(),
                                            description=arg_dict['description'],
                                            comment=arg_dict['comment'])

        # Find and add any child versions as version dependencies:
        dependencies = self.dcc_api.find_references().keys()
        if dependencies:
            version.dependencies = [self.db_api.get_entity(entity='Version', uuid=uuid) for uuid in dependencies]
            self.db_api.publish(entity=version)

        version_file_name = self.db_api.get_version_file_name(version=version)

        published_file = self.db_api.create_entity(entity='File',
                                                   name=version_file_name,
                                                   version=version,
                                                   project=version.project,
                                                   artist=version.artist,
                                                   format=arg_dict['file_format'])

        # Save the file to its VFX work path:
        work_path = self.db_api.get_vfx_path(path_type='work', entity=published_file)

        work_file = self.dcc_api.save_file(file_path=work_path)

        if work_file:
            # Copy the work file to its VFX publish path:
            published_file_path = self.db_api.get_vfx_path(path_type='publish', entity=published_file)
            copyfile(work_file, published_file_path)
            self.logger.info('Created %s resource %s version %s.' % (resource.type,
                                                                     resource.label,
                                                                     version.name))
            info_message(parent=self, msg='Resource version successfully created!')
            self.close()
        else:
            error_message(parent=self, msg='Resource version created, but there was an error saving out its file...')

        self.publish_files(version=version)

        return version, published_file

    def publish_files(self, version):
        """
        Publish files from the file selector.
        :param version: the parent version entity;
        :return: None;
        """
        if not self.file_selector:
            return

        for selected_file in self.file_selector.file_drop.files:
            # File sequence check:
            is_sequence = False
            if '.####.' in selected_file:
                selected_file = selected_file.replace('.####.', '.*.')
                is_sequence = True

            file_name = self.db_api.get_version_file_name(version=version)
            published_file = self.db_api.create_entity(entity='File',
                                                       name=file_name,
                                                       is_sequence=is_sequence,
                                                       version=version,
                                                       project=version.project,
                                                       artist=version.artist,
                                                       format=selected_file.split('.')[-1])

            published_file_path = self.db_api.get_vfx_path(path_type='publish', entity=published_file)

            if is_sequence:
                # Get the appropriate start frame from the resource configuration or from the show configuration:
                set_file_index = STUDIO_CONFIG.data['VFX']['START_FRAME']
                config_data = self.db_api.get_all_config_data(entity=version.resource.entity)
                for i in sorted(config_data.keys()):
                    if 'START_FRAME' in config_data[i]['data']:
                        set_file_index = int(config_data[i]['data']['START_FRAME'])
                        break

                for file in sorted(glob(r'{}'.format(selected_file))):
                    if set_file_index:
                        file_index = set_file_index
                        set_file_index += 1
                    else:
                        file_index = file.split('.')[-2]
                    copyfile(file, published_file_path.replace('.####.', '.{}.'.format(file_index)))
            else:
                copyfile(selected_file, published_file_path)

            self.logger.info('Published file(s) %s from source %s' % (published_file_path, selected_file))

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.publishBtn.clicked.connect(self.publish)
        self.cancelBtn.clicked.connect(self.close)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show(dcc='Maya'):
    """
    Show the UI.
    :return: None;
    """
    publisher = Publisher(dcc=dcc)
    publisher.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
    publisher.setWindowTitle('Piper Publisher {}'.format(__version__))


if __name__ == '__main__':
    run()
