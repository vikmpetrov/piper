# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
from subprocess import Popen
from piper.config import STUDIO_CONFIG, MODULES_PATH_TEMPLATE, ICONS_PATH, DCC_MODULE_VARIABLE
from piper.gui.qt import *
from piper.gui.widgets.app_icon import AppIcon
from piper.gui.widgets.profile_editor import ProfileEditor
from piper.gui.utils import confirm_dialog
from piper.api.logger import get_logger
from piper.api.profiler import Profiler
from piper.api.utils import environment_check
import piper.apps.browser.main as browser
import piper.apps.project_manager.main as project_manager
import piper.apps.task_manager.main as task_manager
import piper.apps.resource_ingestor.main as resource_ingestor
import piper.apps.studio_config.main as studio_config

# List all the available Piper apps:
PIPER_APPS = ['browser', 'project_manager', 'task_manager', 'resource_ingestor', 'studio_config']


class Launcher(QMainWindow):
    """
    An application for configuring and launching software inside the Piper environment.
    """
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        load_qt_ui('launcher.ui', self)
        self.logger = get_logger('Piper Launcher')
        self.profiler = None  # To contain a Profiler instance.
        self.profile = None  # To store the current profile.
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.populate_profiles()
        self.load_profile()

        # Add Piper application icons:
        for app in sorted(PIPER_APPS):
            app_icon = AppIcon(app_name=app,
                               app_bin=eval(app),
                               app_version=eval('{}.__version__'.format(app)),
                               app_type='piper')
            app_icon.clicked.connect(self.start_app)
            self.piperTabLayout.addWidget(app_icon)

    def populate_profiles(self):
        """
        Find all profiles and append them to the profiles combo box.
        :return: None;
        """
        # Block signals while populating with profiles to avoid running any methods:
        self.profilesComboBox.blockSignals(True)
        self.profiler = Profiler()
        self.profilesComboBox.clear()
        for profile in sorted(self.profiler.profiles):
            self.profilesComboBox.addItem(profile)
        # Unblock signals:
        self.profilesComboBox.blockSignals(False)

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.profilesComboBox.currentIndexChanged.connect(self.load_profile)
        self.closeBtn.clicked.connect(self.close)
        self.editProfileBtn.clicked.connect(lambda: self.launch_profile_editor(mode='edit'))
        self.createProfileBtn.clicked.connect(lambda: self.launch_profile_editor(mode='new'))
        self.deleteProfileBtn.clicked.connect(self.delete_profile)

    def load_profile(self):
        """
        Load the current profile file and display its contents.
        :return: None;
        """
        # Clear the DCC app field:
        for i in range(self.dccTabLayout.count()):
            self.dccTabLayout.itemAt(i).widget().deleteLater()
        self.profile = self.profilesComboBox.currentText()
        if self.profile:
            self.logger.debug('Loaded the "%s" profile.' % self.profile)
            profile_data = self.profiler.get_profile_data(self.profile)
            for dcc in sorted(profile_data['software'].keys()):
                if not dcc.upper() in STUDIO_CONFIG.data['SOFTWARE']:
                    continue
                dcc_version = profile_data['software'][dcc]['version']
                dcc_root = STUDIO_CONFIG.data['SOFTWARE'][dcc.upper()]['ROOT'].format(version=dcc_version)
                if os.path.isdir(dcc_root):
                    dcc_bin = os.path.abspath(os.path.join(dcc_root,
                                                           STUDIO_CONFIG.data['SOFTWARE'][dcc.upper()]['BIN'].format(
                                                               version=dcc_version)))
                    dcc_name = dcc.lower()
                    app_icon = AppIcon(app_name=dcc_name, app_version=dcc_version, app_bin=dcc_bin)
                    app_icon.clicked.connect(self.start_app)
                    self.dccTabLayout.addWidget(app_icon)
            # Store the profile in an environment variable for reference outside of this class:
            os.environ['PIPERPROFILE'] = self.profile

    def start_app(self, app_icon):
        """
        Prepare the environment and start an application.
        :param app_icon: the app icon that has been clicked;
        :return: None;
        """
        self.logger.debug('Running %s' % app_icon.app_bin)
        # If the app icon represents a DCC app, set the appropriate environment variables first:
        if app_icon.app_type == 'dcc':
            self.set_environment_varaibles(dcc_name=app_icon.app_name)
            # Start the application as a separate process:
            Popen(app_icon.app_bin, shell=True, close_fds=True, env=os.environ)
        else:
            app_icon.app_bin.show()

    def set_environment_varaibles(self, dcc_name):
        """
        Set the environment variables associated with a given DCC.
        :param dcc_name: the name of the DCC (string);
        :return: None;
        """
        profile_data = self.profiler.get_profile_data(self.profile)
        module_data = profile_data['software'][dcc_name]['modules']
        modules_path = MODULES_PATH_TEMPLATE.format(dcc=dcc_name)

        # Quit if the modules path for the DCC doesn't exist:
        if not os.path.isdir(modules_path):
            return

        # Get the enabled modules only:
        modules = [x for x in module_data if module_data[x] is True]

        # Start with a blank environment:
        os.environ[DCC_MODULE_VARIABLE[dcc_name]] = ''

        # Append the modules' path to the relevant DCC modules environment variable:
        for module in modules:
            if module in os.listdir(modules_path):
                module_root = os.path.abspath(os.path.join(modules_path, module))
                self.logger.debug('Loading module: %s' % module_root)
                if environment_check(DCC_MODULE_VARIABLE[dcc_name]):
                    os.environ[DCC_MODULE_VARIABLE[dcc_name]] = '{0};{1}'.format(
                                                                os.environ[DCC_MODULE_VARIABLE[dcc_name]], module_root)
                else:
                    os.environ[DCC_MODULE_VARIABLE[dcc_name]] = module_root

    def launch_profile_editor(self, mode):
        """
        Launch an instance of the Profile Editor widget.
        :param mode: the mode of the Profile Editor - 'edit' or 'new';
        :return: None;
        """
        profile_name = self.profilesComboBox.currentText()
        profile_file = self.profiler.profiles[profile_name]
        profile_editor = ProfileEditor(parent=self, profile_name=profile_name, profile_file=profile_file, mode=mode)
        profile_editor.exec_()
        # Refresh profiles after the Profile Editor is done editing:
        self.populate_profiles()
        self.profilesComboBox.setCurrentIndex(self.profilesComboBox.findText(self.profile))
        self.load_profile()

    def delete_profile(self):
        """
        Delete the current profile's file.
        :return:
        """
        profile_name = self.profilesComboBox.currentText()
        profile_file = self.profiler.profiles[profile_name]
        if not confirm_dialog(parent=self, question='Are you sure you want to delete profile {}?'.format(profile_name)):
            return
        try:
            os.remove(profile_file)
        except Exception as e:
            self.logger.error('Could not delete profile file %s because of the following exception: %s' % (profile_file,
                                                                                                           e))
        # Refresh profiles:
        self.populate_profiles()
        self.load_profile()


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show()
    ret = app.exec_()
    sys.exit(ret)


def show():
    """
    Show the UI.
    :return: None;
    """
    launcher = Launcher()
    launcher.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
    launcher.setWindowTitle('Piper Launcher {}'.format(__version__))
    launcher.show()


if __name__ == '__main__':
    run()
