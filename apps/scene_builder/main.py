# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
import os
import webbrowser
from glob import glob
from piper.apps import project_manager, task_manager, browser, resource_ingestor
from piper.config import ICONS_PATH
from piper.config.dcc_config import DCC_TO_CATEGORY, DCC_TO_PIPELINE_STEP, DCC_FILE_EXTENTIONS
from piper.gui.qt import QMainWindow, QApplication, QListWidgetItem, QIcon, QTableWidgetItem, QAbstractItemView, \
    QTreeWidgetItem, QColor, QLabel, QComboBox, QPushButton, load_qt_ui
from piper.gui.widgets.create_resource import CreateResource
from piper.api.database_api import DbApi
from piper.api.logger import get_logger
from piper.api.dcc import load_dcc_api
from piper.api.utils import environment_check, browse_path
from piper.gui.widgets.log_work import LogWork
from piper.gui.utils import info_message, error_message, confirm_dialog, set_combo_box_text

TASKS_TABLE_HEADER = ['Project', 'Entity', 'Name', 'Description', 'Pipeline Step', 'Status', 'Start Date', 'Due Date']
ASSEMBLY_TREE_HEADER = ['Entity', 'Resource', 'Description', 'Version', 'Latest', 'Actions']


class SceneBuilder(QMainWindow):
    """
    An object to manage scene assembly in a given DCC.
    """
    def __init__(self, dcc, parent=None):
        QMainWindow.__init__(self, parent)
        load_qt_ui('scene_builder.ui', self)
        self.logger = get_logger('Piper Scene Builder')
        self.logger.info('Starting in %s...' % dcc)
        self.dcc = dcc.lower()
        self.dcc_api = load_dcc_api(self.dcc)
        self._data = {
            'asset_types': {},
            'assets': {},
            'asset_resources': {},
            'shot_resources': {},
            'workspace_resources': {},
            'snapshots': {},
            'tasks': {},
            'assembly_versions': {},
            'shot_versions': {},
            'assembly_items': {}
        }  # To store label-to-entity references.
        self.db_api = DbApi()
        self.artist = self.db_api.get_current_artist()
        self.current_task = None
        self.item_entity_dict = dict()
        if not self.artist:
            error = 'The current user could not be found in the Piper database.'
            error_message(parent=self, msg=error)
            raise(Exception(error))
        else:
            self._setup_data()
            self._connect_widget_cmds()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """
        self.db_api.filters = {'artist': self.artist}
        tasks = [task for task in self.db_api.get_entities(entity='Task', filtered=True)
                 if task.is_active and task.pipeline_step.category == DCC_TO_CATEGORY[self.dcc]
                 and task.pipeline_step.label in DCC_TO_PIPELINE_STEP[self.dcc]]
        self.tasksTableWidget.clear()
        self.tasksTableWidget.setColumnCount(len(TASKS_TABLE_HEADER))
        self.tasksTableWidget.setHorizontalHeaderLabels(TASKS_TABLE_HEADER)
        self.tasksTableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.current_task = self.get_context_task()

        self.tasksTableWidget.setRowCount(len(tasks))
        for i, task in enumerate(tasks):
            self._data['tasks'][i] = task
            self._add_task_to_table_row(row_index=i, task=task)
            if self.current_task and task.uuid == self.current_task.uuid:
                self.tasksTableWidget.selectRow(i)
        self.tasksTableWidget.resizeColumnsToContents()

        self.assemblyTreeWidget.setHeaderItem(QTreeWidgetItem(ASSEMBLY_TREE_HEADER))
        self.refresh_tree_data()

        self.re_populate_all()

    def refresh_tree_data(self):
        """
        Setup the asset tree data.
        :return: None;
        """
        self.assemblyTreeWidget.clear()
        self.build_assembly_tree(referenced_version_uuids=self.dcc_api.find_references(),
                                 parent_item=self.assemblyTreeWidget)
        # Show all of the fields in the tree:
        self.assemblyTreeWidget.expandAll()

    def build_assembly_tree(self, referenced_version_uuids, parent_item):
        """
        Represent the database as items in the UI's data tree.
        :param referenced_version_uuids: a hirerarchical dictionary of UUIDs of versions present in the scene;
        :param parent_item: the parent of the generated QTreeWidgetItem;
        :return: None;
        """
        for version_uuid in referenced_version_uuids:
            item = self.append_to_assembly_tree(parent_item=parent_item,
                                                entity=self.db_api.get_entity('Version', uuid=version_uuid))
            self.build_assembly_tree(referenced_version_uuids=referenced_version_uuids[version_uuid], parent_item=item)

    def append_to_assembly_tree(self, parent_item, entity):
        """
        Add a data row to the data tree and store its main item under a unique ID for future reference.
        :param parent_item: the parent item of the item to be created;
        :param entity: the entity to be represented;
        :return: a newly created data tree item;
        """

        item = QTreeWidgetItem(parent_item)

        version_combo_box = QComboBox(self.assemblyTreeWidget)
        for version in reversed(entity.resource.versions):
            version_combo_box.addItem(version.label)
        set_combo_box_text(combo_box=version_combo_box, text=entity.label)
        is_latest_label = QLabel(self.assemblyTreeWidget)

        remove_btn = QPushButton('Remove', self.assemblyTreeWidget)

        self.assemblyTreeWidget.setItemWidget(item, 0, QLabel('{0} {1}'.format(entity.resource.parent.entity.type,
                                                                               entity.resource.parent.entity.label),
                                                              self.assemblyTreeWidget))
        self.assemblyTreeWidget.setItemWidget(item, 1, QLabel(entity.resource.label,
                                                              self.assemblyTreeWidget))
        self.assemblyTreeWidget.setItemWidget(item, 2, QLabel('{}'.format(entity.description), self.assemblyTreeWidget))
        self.assemblyTreeWidget.setItemWidget(item, 3, version_combo_box)
        self.assemblyTreeWidget.setItemWidget(item, 4, is_latest_label)
        self.assemblyTreeWidget.setItemWidget(item, 5, remove_btn)

        self._assembly_item_version_check(item=item, label=is_latest_label, version=entity)

        item_id = self.assemblyTreeWidget.indexFromItem(item, 0).internalId()
        self._data['assembly_versions'][entity.uuid] = {
            'item': item,
            'entity': entity
        }

        self._data['assembly_items'][item_id] = {'entity': entity, 'item': item, 'version': entity.label}

        remove_btn.clicked.connect(lambda: self.remove_assembly_tree_item(item=item, version=entity))
        version_combo_box.currentIndexChanged.connect(lambda: self.update_assembly_item_version(item_id=item_id))
        item.setExpanded(True)
        return item

    def remove_assembly_tree_item(self, item, version):
        """
        Remove an item from the assembly tree as well as its corresponding reference file in the scene.
        :param item: the target item;
        :param version: a version entity;
        :return: None;
        """
        if confirm_dialog(parent=self, question='Are you sure you want to remove this version?'):
            invisible_root = self.assemblyTreeWidget.invisibleRootItem()
            (item.parent() or invisible_root).removeChild(item)
            for file in version.files:
                self.dcc_api.remove_reference(file)
            self._data['assembly_versions'].pop(version.uuid, None)

    def update_assembly_item_version(self, item_id):
        """
        Update an version item's version and replace its corresponding reference file in the scene.
        :param item_id: the (str) unique ID of the target item;
        :return: an info message if successful, otherwise - an error message;
        """
        # Get the relevant data:
        resource = self._data['assembly_items'][item_id]['entity'].resource
        item = self._data['assembly_items'][item_id]['item']
        parent_item_entity = None
        if item.parent():
            parent_id = self.assemblyTreeWidget.indexFromItem(item.parent(), 0).internalId()
            if parent_id in self._data['assembly_items']:
                parent_item_entity = self._data['assembly_items'][parent_id]['entity']
        label = self.assemblyTreeWidget.itemWidget(item, 4)
        version_combo_box = self.assemblyTreeWidget.itemWidget(item, 3)
        description = self.assemblyTreeWidget.itemWidget(item, 2)

        # Get the previous and new versions:
        self.db_api.filters['resource'] = resource
        self.db_api.filters['label'] = self._data['assembly_items'][item_id]['version']
        previous_version = self.db_api.get_entities(entity='Version', filtered=True)[0]
        
        self.db_api.filters['resource'] = resource
        self.db_api.filters['label'] = version_combo_box.currentText()
        selected_version = self.db_api.get_entities(entity='Version', filtered=True)[0]

        # Replace the previous version's referenced publish file in the scene with the new one:
        new_version_file = selected_version.files[0]
        new_version_file.path = self.db_api.get_vfx_path(path_type='publish', entity=selected_version.files[0])
        self._file_entity_range_check(new_version_file)
        if not self.dcc_api.replace_reference(version_item_entity=previous_version,
                                              parent_item_entity=parent_item_entity,
                                              new_version_file=new_version_file):
            version_combo_box.blockSignals(True)
            set_combo_box_text(combo_box=version_combo_box, text=self._data['assembly_items'][item_id]['version'])
            version_combo_box.blockSignals(False)
            return error_message(parent=self, msg='Resource {0} version {1} replacement to {2} unsuccessful!'.format(
                resource.label, previous_version.label, selected_version.label))
        else:
            self._data['assembly_items'][item_id]['entity'] = selected_version

        referenced_version_uuids = self.dcc_api.find_references(root=selected_version)

        # Remove all item children and rebuild its assembly tree:
        item.takeChildren()

        # Build the new version's assembly tree branch with any child references:
        self.build_assembly_tree(referenced_version_uuids=referenced_version_uuids,
                                 parent_item=item)

        self._data['assembly_items'][item_id]['version'] = selected_version.label
        self._data['assembly_versions'].pop(previous_version.uuid, None)
        self._assembly_item_version_check(item=item, label=label, version=selected_version)
        description.setText(selected_version.description)

        return info_message(parent=self, msg='Resource {0} version {1} replaced with {2}!'.format(
                resource.label, previous_version.label, selected_version.label))

    @staticmethod
    def _assembly_item_version_check(item, label, version):
        """
        Check if the given version is the latest and reflect that in its item.
        :param item: the item of the version's "latest" cell;
        :param label: the label of the version's "latest" cell;
        :param version: a version entiy;
        :return: None;
        """
        if version.is_latest:
            color = QColor(0, 128, 0)
        else:
            color = QColor(220, 20, 60)
        item.setBackgroundColor(4, color)
        label.setText(str(version.is_latest))

    def _add_task_to_table_row(self, row_index, task):
        """
        Represent a given task as a table row in the tasks table widget.
        :param row_index: the (int) row index of the new table row;
        :param task: a task entity;
        :return: None;
        """
        self.tasksTableWidget.setItem(row_index, 0, QTableWidgetItem(task.project.label))
        self.tasksTableWidget.setItem(row_index, 1, QTableWidgetItem('{0}_{1}'.format(task.entity.sequence.label,
                                                                                      task.entity.label)))
        self.tasksTableWidget.setItem(row_index, 2, QTableWidgetItem(task.label))
        self.tasksTableWidget.setItem(row_index, 3, QTableWidgetItem(task.description))
        self.tasksTableWidget.setItem(row_index, 4, QTableWidgetItem(task.pipeline_step.label))
        self.tasksTableWidget.setItem(row_index, 5, QTableWidgetItem(task.status.label))
        self.tasksTableWidget.setItem(row_index, 6, QTableWidgetItem(task.start_date.strftime("%Y-%m-%d")))
        self.tasksTableWidget.setItem(row_index, 7, QTableWidgetItem(task.end_date.strftime("%Y-%m-%d")))

    def set_task_environment(self, index):
        """
        Set the current task environment based on the currently selected task table cell.
        :param index: the (int) index of the currently selected task table cell;
        :return: None;
        """
        self.current_task = self._data['tasks'][index]
        os.environ['PIPER_TASK_UUID'] = self.current_task.uuid
        self.logger.info('Set Piper context from task UUID: %s' % self.current_task.uuid)
        self.populate_assets()
        self.populate_workspace_resources()
        self.populate_shot_resources()

    def re_populate_all(self):
        """
        Re-populate various UI elements with data.
        :return: None;
        """
        self.populate_workspace_resources()
        self.populate_workspace_versions()
        self.populate_snapshots()
        self.populate_asset_types()
        self.populate_assets()
        self.populate_asset_resources()
        self.populate_asset_resource_versions()
        self.populate_shot_resources()
        self.populate_shot_resource_versions()

    def get_context_task(self):
        """
        Get the environment context task.
        :return: a task entity if found, otherwise - None;
        """
        if environment_check('PIPER_TASK_UUID'):
            return self.db_api.get_entity(entity='Task', uuid=os.environ['PIPER_TASK_UUID'])
        return None

    def populate_asset_types(self):
        """
        Populate with asset types.
        :return:
        """
        self.assetTypeComboBox.clear()
        asset_types = self.db_api.get_entities(entity='AssetType', filtered=True)
        for asset_type in asset_types:
            self.assetTypeComboBox.addItem(asset_type.label)
            self._data['asset_types'][asset_type.label] = asset_type

    def populate_assets(self):
        """
        Populate with assets of the currently selected asset type.
        :return: None;
        """
        self.assetComboBox.clear()
        self._data['assets'] = {}
        if self.current_task:
            asset_type = self._data['asset_types'][self.assetTypeComboBox.currentText()]
            self.db_api.filters['asset_type'] = asset_type
            self.db_api.filters['project'] = self.current_task.project
            assets = self.db_api.get_entities(entity='Asset', filtered=True)
            for asset in assets:
                self._data['assets'][asset.label] = asset
                self.assetComboBox.addItem(asset.label)

    def populate_shot_resources(self):
        """
        Populate with shot resources of the currently selected task's shot if applicable.
        :return: None;
        """
        self.shotResourceComboBox.clear()
        self._data['shot_resources'] = {}
        if self.current_task and self.current_task.shot:
            for resource in self.current_task.shot.resources:
                if resource.category == DCC_TO_CATEGORY[self.dcc]:
                    self._data['shot_resources'][resource.label] = {'entity': resource, 'versions': {}}
                    self.shotResourceComboBox.addItem(resource.label)

    def populate_asset_resources(self):
        """
        Add resources to the assembly asset UI elements.
        :return: None;
        """
        self.assetResourceComboBox.clear()
        self._data['asset_resources'] = {}
        selected_asset = self.assetComboBox.currentText()
        if selected_asset:
            # Get the selected asset's resources:
            asset_entity = self._data['assets'][selected_asset]
            self.db_api.session.refresh(asset_entity)
            for resource_entity in asset_entity.resources:
                if resource_entity.category == DCC_TO_CATEGORY[self.dcc]:
                    # Store the resource in the context data dictionary for future reference:
                    self._data['asset_resources'][resource_entity.label] = {'entity': resource_entity, 'versions': {}}
                    self.assetResourceComboBox.addItem(resource_entity.label)

    def populate_shot_resource_versions(self):
        """
        Add resource versions to the assembly shot UI elements.
        :return: None;
        """
        self.shotVersionComboBox.clear()
        selected_resource = self.shotResourceComboBox.currentText()
        if selected_resource:
            # Get the selected resource's versions:
            resource_entity = self._data['shot_resources'][selected_resource]['entity']
            self.db_api.session.refresh(resource_entity)
            for version_entity in reversed(resource_entity.versions):
                # Store the version in the context data dictionary for future reference:
                self._data['shot_resources'][resource_entity.label]['versions'][version_entity.label] = version_entity
                self.shotVersionComboBox.addItem(version_entity.label)

    def populate_asset_resource_versions(self):
        """
        Add resource versions to the assembly asset UI elements.
        :return: None;
        """
        self.assetVersionComboBox.clear()
        selected_resource = self.assetResourceComboBox.currentText()
        if selected_resource:
            # Get the selected resource's versions:
            resource_entity = self._data['asset_resources'][selected_resource]['entity']
            self.db_api.session.refresh(resource_entity)
            for version_entity in reversed(resource_entity.versions):
                # Store the version in the context data dictionary for future reference:
                self._data['asset_resources'][resource_entity.label]['versions'][version_entity.label] = \
                                                                                        version_entity
                self.assetVersionComboBox.addItem(version_entity.label)

    def populate_workspace_resources(self):
        """
        Add resources to the workspace area of the UI.
        :return: None;
        """
        self.resourceList.clear()
        self.versionList.clear()
        self._data['workspace_resources'] = {}

        current_task = self.get_context_task()
        if current_task:
            resources = [r for r in current_task.entity.resources if r.task == self.current_task]
            for resource in resources:
                if resource.category == DCC_TO_CATEGORY[self.dcc]:
                    item = QListWidgetItem(resource.label)
                    self.resourceList.addItem(item)
                    # Store the resource in the context data dictionary for future reference:
                    self._data['workspace_resources'][resource.label] = {'entity': resource, 'versions': {}}

    def populate_workspace_versions(self):
        """
        Add the currently selected resource's versions to the workspace area of the UI.
        :return: None;
        """
        self.versionList.clear()

        selected_resource_item = self.resourceList.currentItem()
        if selected_resource_item:
            resource_entity = self._data['workspace_resources'][selected_resource_item.text()]['entity']
            os.environ['PIPER_RESOURCE_UUID'] = resource_entity.uuid
            self.logger.info(msg='Set context resource UUID: %s' % resource_entity.uuid)
            # Get the selected resource's versions:
            if resource_entity.versions:
                for version in reversed(resource_entity.versions):
                    # Store the version in the context data dictionary for future reference:
                    self._data['workspace_resources'][selected_resource_item.text()][
                        'versions'][version.label] = version
                    item = QListWidgetItem(version.label)
                    self.versionList.addItem(item)
                os.environ['PIPER_VERSION_UUID'] = resource_entity.versions[-1].uuid

    def populate_snapshots(self):
        """
        Find all snapshots of the currently selected version and represent them in the workspace area of the UI.
        :return: None;
        """
        self.snapshotList.clear()
        self._data['snapshots'] = {}

        selected_resource_item = self.resourceList.currentItem()
        selected_version_item = self.versionList.currentItem()

        if selected_version_item:
            version_entity = self._data['workspace_resources'][selected_resource_item.text()][
                                        'versions'][selected_version_item.text()]
            os.environ['PIPER_VERSION_UUID'] = version_entity.uuid
            self.logger.info(msg='Set context version UUID: %s' % version_entity.uuid)
            version_path = self.db_api.get_vfx_path(entity=version_entity, path_type='work')
            if version_path:
                snapshot_path = os.path.join(version_path, '_snapshots')
                if os.path.isdir(snapshot_path):
                    snapshots = os.listdir(snapshot_path)
                    snapshots.reverse()
                    for snapshot in snapshots:
                        # Store snapshot in the context data dictionary for future reference:
                        self._data['snapshots'][snapshot] = os.path.join(snapshot_path, snapshot)
                        self.snapshotList.addItem(snapshot)

    def run_create_resource(self):
        """
        Instantiate the Create Work File widget.
        :return: Error message if lacking valid input, otherwise - None;
        """
        if self.current_task:
            create_resource_dialog = CreateResource(parent=self)
            create_resource_dialog.exec_()
            # Refresh workspace resources after creating a work file:
            self.populate_workspace_resources()
            self.populate_assets()
        else:
            return error_message(parent=self, msg='No task selected!')

    def find_asset(self):
        """
        Find the selected asset/resource/version's full publish path and open it in the file explorer if it exists.
        :return: None;
        """
        selected_version = self.assetVersionComboBox.currentText()
        selected_resource = self.assetResourceComboBox.currentText()
        # Determine which entity to locate based on which asset/resource/version is selected:
        if not selected_version:
            if not selected_resource:
                selected_asset = self.assetComboBox.currentText()
                if not selected_asset:
                    return
                else:
                    entity = self._data['assets'][selected_asset]
            else:
                entity = self._data['asset_resources'][selected_resource]['entity']
        else:
            entity = self._data['asset_resources'][selected_resource]['versions'][selected_version]

        self._entity_path_check(entity=entity)

    def find_shot(self):
        """
        Find the selected shot/resource/version's full publish path and open it in the file explorer if it exists.
        :return: None;
        """
        selected_version = self.shotVersionComboBox.currentText()
        selected_resource = self.shotResourceComboBox.currentText()
        # Determine which entity to locate based on which asset/resource/version is selected:
        if not selected_version:
            if not selected_resource:
                return
            else:
                entity = self._data['shot_resources'][selected_resource]['entity']
        else:
            entity = self._data['shot_resources'][selected_resource]['versions'][selected_version]

        self._entity_path_check(entity=entity)

    def _entity_path_check(self, entity):
        """
        Check if an entity's published path exists and open it if it does.
        :param entity: a database entity;
        :return: None;
        """
        path = self.db_api.get_vfx_path(path_type='publish', entity=entity)
        if os.path.isdir(path):
            browse_path(os.path.abspath(path))
        else:
            self.logger.error("This entity's VFX publish path %s does not exist." % path)

    def open_work_file_version(self):
        """
        Attempt to open the selected work file version inside the current DCC.
        :return: None;
        """
        version_entity = self.db_api.get_entity('Version', uuid=os.environ['PIPER_VERSION_UUID'])

        if version_entity:
            if not version_entity.files[0].format in DCC_FILE_EXTENTIONS[self.dcc]:
                return error_message(parent=self, msg='{0} cannot open "{1}" file types.'.format(self.dcc.title(),
                                                                                  version_entity.files[0].format))
            path = self.db_api.get_vfx_path(path_type='work', entity=version_entity.files[0])
            # Try to open the version file:
            result = self.dcc_api.open_file(path)
            if not result:
                return error_message(parent=self, msg='Could not find version file: {}'.format(path))

            self.refresh_tree_data()

    def open_snapshot(self):
        """
        Open the currently selected snapshot file.
        :return: None;
        """
        selected_snapshot_item = self.snapshotList.currentItem()
        if selected_snapshot_item:
            self.dcc_api.open_file(self._data['snapshots'][selected_snapshot_item.text()])
            self.refresh_tree_data()

    def _set_widget_signal_block(self, block):
        # Block/unblock all relevant widget signals:
        self.assetTypeComboBox.blockSignals(block)
        self.assetComboBox.blockSignals(block)
        self.assetResourceComboBox.blockSignals(block)
        self.resourceList.blockSignals(block)
        self.versionList.blockSignals(block)

    def refresh_ui(self):
        """
        Update all database data contained in various UI elements.
        :return: None;
        """
        # Block widget signals:
        self._set_widget_signal_block(block=True)
        # Repopulate the UI:
        self._setup_data()
        self.logger.debug('UI refreshed.')
        # Unblock all blocked signals:
        self._set_widget_signal_block(block=False)

    def create_snapshot(self):
        """
        Snapshot the current state of the scene file.
        :return:
        """
        self.dcc_api.snapshot()
        self.populate_snapshots()

    def add_assembly_version(self, type):
        """
        Add the currently selected asset/shot resource version to the scene.
        :param type: the (str) resource type to query - 'asset_resources' or 'shot_resources';
        :return: An error message if unsuccessful, otherwise - None;
        """
        if type == 'asset_resources':
            version_name = self.assetVersionComboBox.currentText()
            resource_name = self.assetResourceComboBox.currentText()
        else:
            version_name = self.shotVersionComboBox.currentText()
            resource_name = self.shotResourceComboBox.currentText()

        if resource_name and version_name:
            version = self._data[type][resource_name]['versions'][version_name]
            # Check if the corresponding resource is already in the scene:
            if self.dcc_api.find_node_from_uuid(uuid_attribute='piper_resource_uuid', uuid_value=version.resource.uuid):
                return error_message(parent=self,
                                     msg='A {} version is already present!'.format(version.resource.label))

            file_entity = version.files[0]
            file_entity.path = self.db_api.get_vfx_path(path_type='publish', entity=file_entity)
            self._file_entity_range_check(file_entity)
            self.dcc_api.reference_version_file(file_entity)
            referenced_version_uuids = self.dcc_api.find_references()
            item = self.append_to_assembly_tree(parent_item=self.assemblyTreeWidget, entity=version)
            # Add any child references to the newly added assembly tree version item:
            self.build_assembly_tree(referenced_version_uuids=referenced_version_uuids[version.uuid], parent_item=item)

    @staticmethod
    def _file_entity_range_check(file_entity):
        """
        Check if a given file entity is a sequence and if so determine and store its range.
        :param file_entity: a file entity;
        :return: None;
        """
        if file_entity.is_sequence:
            sequence = sorted(glob(os.path.join(os.path.dirname(file_entity.path),
                                                '*.{}'.format(file_entity.format))))
            file_entity.range = [int(sequence[0].split('.')[-2]), int(sequence[-1].split('.')[-2])]

    def log_work(self):
        """
        Run the log work dialog.
        :return: None if successful, otherwise - an error message.
        """
        if environment_check('PIPER_TASK_UUID'):
            log_work = LogWork(parent=self)
            log_work.exec_()
        else:
            return error_message(parent=self, msg='There is no task currently being worked on!')

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.tasksTableWidget.cellClicked.connect(self.set_task_environment)
        self.assetTypeComboBox.currentIndexChanged.connect(self.populate_assets)
        self.assetComboBox.currentIndexChanged.connect(self.populate_asset_resources)
        self.assetResourceComboBox.currentIndexChanged.connect(self.populate_asset_resource_versions)
        self.actionNewResource.triggered.connect(self.run_create_resource)
        self.actionSnapshot.triggered.connect(self.create_snapshot)
        self.actionProjectManager.triggered.connect(project_manager.show)
        self.actionTaskManager.triggered.connect(task_manager.show)
        self.actionBrowser.triggered.connect(browser.show)
        self.actionResourceIngestor.triggered.connect(resource_ingestor.show)
        self.actionWorkLog.triggered.connect(self.log_work)
        self.resourceList.currentItemChanged.connect(self.populate_workspace_versions)
        self.resourceList.itemDoubleClicked.connect(self.open_work_file_version)
        self.versionList.itemDoubleClicked.connect(self.open_work_file_version)
        self.versionList.currentItemChanged.connect(self.populate_snapshots)
        self.snapshotList.itemDoubleClicked.connect(self.open_snapshot)
        self.findAssetBtn.clicked.connect(self.find_asset)
        self.findShotBtn.clicked.connect(self.find_shot)
        self.refreshBtn.clicked.connect(self.refresh_ui)
        self.assetVersionAddBtn.clicked.connect(lambda: self.add_assembly_version(type='asset_resources'))
        self.shotVersionAddBtn.clicked.connect(lambda: self.add_assembly_version(type='shot_resources'))
        self.shotResourceComboBox.currentIndexChanged.connect(self.populate_shot_resource_versions)
        self.closeBtn.clicked.connect(self.close)


def run(dcc='Maya'):
    """
    Create and run the application.
    :return: None;
    """
    app = QApplication(sys.argv)
    show(dcc=dcc)
    sys.exit(app.exec_())


def show(dcc):
    """
    Show the UI.
    :return: None;
    """
    scene_builder = SceneBuilder(dcc=dcc)
    scene_builder.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
    scene_builder.setWindowTitle('Piper Scene Builder {}'.format(__version__))
    scene_builder.show()


if __name__ == '__main__':
    run()
