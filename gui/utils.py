# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

from piper.gui.qt import Qt, QMessageBox, QFileDialog


def warning_message(parent, msg):
    """
    Shows a warning message.
    :param parent: the UI parent of this message;
    :param msg: the (str) message to be displayed;
    :return: True;
    """
    QMessageBox.warning(parent, "Piper - Warning", msg)
    parent.logger.warn(msg=msg)
    return True


def info_message(parent, msg):
    """
    Shows an information message.
    :param parent: the UI parent of this message;
    :param msg: the (str) message to be displayed;
    :return: True;
    """
    QMessageBox.information(parent, "Piper - Info", msg)
    parent.logger.info(msg=msg)
    return True


def error_message(parent, msg):
    """
    Show the critical message.
    :param parent: the UI parent of this message;
    :param msg: the (str) message to be displayed;
    :return: True;
    """
    QMessageBox.critical(parent, "Piper - Error", msg)
    parent.logger.error(msg=msg)
    return True


def confirm_dialog(parent, question):
    """
    Shows a confirmation dialog.
    :param parent: the UI parent of this message;
    :param question: the (str) question to be displayed;
    :return: True if confirmed, otherwise - False;
    """
    response = QMessageBox.question(parent,
                                    "Piper - Confirm",
                                    question,
                                    QMessageBox.Yes,
                                    QMessageBox.No)
    if response == QMessageBox.Yes:
        parent.logger.debug(msg='"%s" confirmed!' % question)
        return True
    else:
        parent.logger.debug(msg='"%s" rejected!' % question)
        return False


def set_combo_box_text(combo_box, text):
    """
    Attempt to set the text of a given combo box to some specific text.
    :param combo_box: the combo box to set the text of;
    :param text: the text to look for in the combo box's items (string);
    :return: None;
    """
    index = combo_box.findText(text, Qt.MatchFixedString)
    if index >= 0:
        combo_box.setCurrentIndex(index)


def browse_file_dialog(parent, caption='Select file', filters=None):
    """
    Create a file browser dialog.
    :param parent: the parent Qt window/widget;
    :param caption: a (str) caption for the file browser dialog;
    :param filters: (str) file extension filters;
    :return: the (str) path of the selected file;
    """
    file_name, _ = QFileDialog.getOpenFileNames(parent, caption, filter=filters)
    parent.logger.debug(msg='Selected file: %s' % file_name)
    return file_name
