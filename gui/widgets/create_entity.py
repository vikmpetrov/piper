# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.config import ICONS_PATH
from piper.gui.qt import QDialog, QIcon, load_qt_ui
from piper.gui.utils import info_message, error_message, set_combo_box_text
from piper.api.database_api import DbApi
from piper.api.logger import get_logger


class CreateEntity(QDialog):
    """
    A widget used to create work file resources in the database.
    """

    def __init__(self, parent, entity_type, entity=None, edit_mode=False):
        QDialog.__init__(self, parent)
        self.logger = get_logger('Piper Create Entity')
        load_qt_ui('create_entity.ui', self)
        self.parent = parent
        self.entity_type = entity_type
        self.entity = entity
        self.edit_mode = edit_mode
        self.move(self.parent.pos())  # Move this widget to where its parent window is.
        self.db_api = DbApi()
        self._data = {
            'department': {},
            'status': {}
        }
        entity_hide_widgets = {
            'Artist': [],
            'Department': [self.administratorWidget,
                           self.departmentWidget]
        }
        for widget in entity_hide_widgets[self.entity_type]:
            widget.setHidden(True)
        self._setup_data()
        self._connect_widget_cmds()
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
        if edit_mode:
            self.headerLabel.setText('Updating {}'.format(self.entity_type))
            self.setWindowTitle('Piper Update {}'.format(self.entity_type))
            self.createBtn.setText('Update {}'.format(self.entity_type.lower()))
            self.setWindowTitle('Piper Update {}'.format(self.entity_type))
        else:
            self.headerLabel.setText('Creating {}'.format(self.entity_type))
            self.setWindowTitle('Piper Create {}'.format(self.entity_type))
            self.createBtn.setText('Create {}'.format(self.entity_type.lower()))

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.db_api.filters = {'applies_to': 'All'}
        for status in self.db_api.get_entities(entity='Status', filtered=True):
            self.statusComboBox.addItem(status.label)
            self._data['status'][status.label] = status

        if not self.departmentWidget.isHidden():
            for department in self.db_api.get_entities(entity='Department'):
                self.departmentComboBox.addItem(department.label)
                self._data['department'][department.label] = department

        if self.edit_mode:
            self.nameLineEdit.setText(self.entity.label)
            self.commentLineEdit.setText(self.entity.comment)
            self.descriptionLineEdit.setText(self.entity.description)
            self.emailLineEdit.setText(self.entity.email)

            if self.entity_type == 'Artist':
                set_combo_box_text(combo_box=self.statusComboBox, text=self.entity.status.label)
                set_combo_box_text(combo_box=self.departmentComboBox, text=self.entity.department.label)

    def create_entity(self):
        """
        Create a new entity based on the user's input.
        :return: None;
        """
        name = self.nameLineEdit.text()
        if not name:
            return error_message(parent=self, msg='Please enter a valid name!')

        if not self.edit_mode:
            if self.entity_type == 'Artist':
                self.entity = self.db_api.create_entity(entity=self.entity_type,
                                                        name=name,
                                                        department=self._data['department'][
                                                            self.departmentComboBox.currentText()])
            else:
                self.entity = self.db_api.create_entity(entity=self.entity_type, name=name)

        if not self.entity:
            return error_message(parent=self, msg='{0} {1} already exists!'.format(self.entity_type, name))

        self.entity.name = name
        self.entity.label = name
        self.entity.status = self._data['status'][self.statusComboBox.currentText()]
        self.entity.email = self.emailLineEdit.text()
        self.entity.description = self.descriptionLineEdit.text()
        self.entity.comment = self.commentLineEdit.text()

        if self.entity_type == 'Artist':
            administrator = self.isAdministratorComboBox.currentText()
            if administrator == 'Yes':
                is_administrator = True
            else:
                is_administrator = False

            self.entity.is_administrator = is_administrator
            self.entity.department = self._data['department'][self.departmentComboBox.currentText()]

        self.db_api.publish(entity=self.entity)

        if self.edit_mode:
            msg = '{0} {1} successfully updated!'.format(self.entity.type, self.entity.label)
        else:
            msg = '{0} {1} successfully created!'.format(self.entity.type, self.entity.label)

        info_message(parent=self, msg=msg)
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.createBtn.clicked.connect(self.create_entity)
