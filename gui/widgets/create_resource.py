# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.config import ICONS_PATH
from piper.gui.qt import QDialog, QLabel, QIcon, load_qt_ui
from piper.gui.utils import error_message
from piper.api.database_api import DbApi
from piper.config.dcc_config import DCC_FILE_EXTENTIONS
from piper.api.logger import get_logger
from piper.api.profiler import Profiler
from piper.api.dcc import load_dcc_api
from piper.apps.publisher import Publisher


class CreateResource(QDialog):
    """
    A widget used to create work file resources in the database.
    """
    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.logger = get_logger('Piper Create Work File')
        load_qt_ui('create_resource.ui', self)
        self.parent = parent
        self.move(self.parent.pos())  # Move this widget to where its parent window is.
        self.dcc_api = load_dcc_api(dcc=self.parent.dcc)  # Get the relevant DCC API.
        self.profiler = Profiler()
        self.file_prefix = None
        self.db_api = DbApi()
        self._setup_data()
        self._connect_widget_cmds()
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
        self.setWindowTitle('Piper Create Resource {}'.format(__version__))

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.contextLayout.addWidget(QLabel('Project: {}'.format(self.parent.current_task.project.label)))
        entity_name = '{0}_{1}'.format(self.parent.current_task.entity.sequence.name,
                                       self.parent.current_task.entity.name)
        self.contextLayout.addWidget(QLabel('Entity: {0}'.format(entity_name)))
        self.file_prefix = entity_name

        self.pipelineStepLineEdit.setText(self.parent.current_task.pipeline_step.label)

        for file_extention in DCC_FILE_EXTENTIONS[self.parent.dcc]:
            self.fileExtentionComboBox.addItem(file_extention)

        return True

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.createResourceBtn.clicked.connect(self.create_workfile)

    def create_workfile(self):
        """
        Create a new resource and its initial version in the database.
        :return: False if the resource already exists, error message if the user and/or proposed name is invalid,
        otherwise - True;
        """
        resource_name = self.resourceNameLineEdit.text()

        # Sanity check the resource name:
        if not resource_name:
            return error_message(parent=self, msg='Please set a resource name in order to create it.')

        resource = self.db_api.create_entity(entity='Resource',
                                             task=self.parent.current_task,
                                             project=self.parent.current_task.project,
                                             name='{0}_{1}'.format(self.parent.current_task.pipeline_step.name,
                                                                   resource_name),
                                             source=self.parent.dcc,
                                             artist=self.parent.artist,
                                             description=self.descriptionLineEdit.text(),
                                             comment=self.commentLineEdit.text())

        if not resource:
            error_message(parent=self, msg='This resource already exists, creation failed.')
            return False

        # Check whether to create the resource version file from the current file or from the profile's startup file:
        from_current_file = self.fromCurrentFileRadioBtn.isChecked()
        if not from_current_file:
            profile_data = self.profiler.get_profile_data(os.environ['PIPERPROFILE'])
            # Get the profile's startup file:
            if 'startup_file' in profile_data['software'][self.parent.dcc]:
                startup_file = os.path.join(profile_data['startup_directory'],
                                            profile_data['software'][self.parent.dcc]['startup_file'])
                if os.path.isfile(startup_file):
                    self.dcc_api.open_file(startup_file)
                else:
                    self.logger.error('Could not find the startup file %s specified in this profile.' % startup_file)

        os.environ['PIPER_RESOURCE_UUID'] = resource.uuid
        self.logger.info(msg='Set context resource UUID: %s' % resource.uuid)
        publisher = Publisher(dcc=self.parent.dcc, ui=False)
        arg_dict = {'description': self.descriptionLineEdit.text(),
                    'comment': self.commentLineEdit.text(),
                    'file_format': self.fileExtentionComboBox.currentText()}
        publisher.publish(arg_dict=arg_dict)
        self.close()
