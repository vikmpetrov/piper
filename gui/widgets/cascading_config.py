# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.gui.qt import QApplication, QDialog, load_qt_ui, QTableWidgetItem, QAbstractItemView, QColor, QIcon
from piper.config import ICONS_PATH
from piper.gui.utils import info_message
from piper.api.database_api import DbApi
from piper.api.utils import string_to_json
from piper.api.logger import get_logger
from piper.api.utils import environment_check
from piper.database.entities import Task
from piper.api.database_connection import DbConnection


class CascadingConfig(QDialog):
    """
    An widget for viewing and editing configuration data for a given entity.
    """
    def __init__(self, entity, mode='config', parent=None):
        QDialog.__init__(self, parent)
        self.logger = get_logger('Piper Cascading Config')
        load_qt_ui('cascading_config.ui', self)
        self.entity = entity
        self.entityLabel.setText(entity.name)
        self.db_api = DbApi()
        self._setup_data()
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
        self.setWindowTitle('Piper Cascading Config {}'.format(__version__))
        if parent:
            self.move(parent.pos())
        if mode != 'config':
            self.closeBtn.setHidden(True)
            self.applyBtn.setHidden(True)
            self.addAttributeBtn.setHidden(True)
            self.deleteSelectedBtn.setHidden(True)
            self.ownConfigTable.setEditTriggers(QAbstractItemView.NoEditTriggers)
        else:
            self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        # Collect all inherited config data:
        parent_config_data = self.db_api.get_all_config_data(entity=self.entity.parent)

        # Represent the collected parent config data in the respective table:
        if parent_config_data:
            self.inheritedConfigTable.setColumnCount(4)
            inherited_conifg_count = 0
            attr_list = []
            index = 0
            for i, configData in sorted(parent_config_data.items()):
                inherited_conifg_count += len(configData['data'])
                self.inheritedConfigTable.setRowCount(inherited_conifg_count)
                for attr, value in configData['data'].items():
                    self.inheritedConfigTable.setItem(index, 0, QTableWidgetItem(configData['name']))
                    self.inheritedConfigTable.setItem(index, 1, QTableWidgetItem(configData['type']))
                    self.inheritedConfigTable.setItem(index, 2, QTableWidgetItem(str(attr)))
                    self.inheritedConfigTable.setItem(index, 3, QTableWidgetItem(str(value)))
                    if attr in attr_list:
                        self.inheritedConfigTable.item(index, 2).setBackground(QColor(204, 0, 0))
                        self.inheritedConfigTable.item(index, 3).setBackground(QColor(204, 0, 0))
                    else:
                        self.inheritedConfigTable.item(index, 2).setBackground(QColor(0, 153, 0))
                        self.inheritedConfigTable.item(index, 3).setBackground(QColor(0, 153, 0))
                        attr_list.append(attr)
                    index += 1

        self.inheritedConfigTable.resizeColumnsToContents()
        self.inheritedConfigTable.resizeRowsToContents()
        self.inheritedConfigTable.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # Get own config data and represent it in the respective table:
        own_config = self.db_api.get_config(entity=self.entity)

        if own_config:
            own_config_data = string_to_json(own_config.config_rules)
            self.ownConfigTable.setColumnCount(2)
            self.ownConfigTable.setRowCount(len(own_config_data))
            index = 0
            for attr in sorted(own_config_data):
                self.ownConfigTable.setItem(index, 0, QTableWidgetItem(str(attr)))
                self.ownConfigTable.setItem(index, 1, QTableWidgetItem(str(own_config_data[attr])))
                index += 1

        self.ownConfigTable.resizeColumnsToContents()
        self.inheritedConfigTable.resizeColumnsToContents()

    def add_attribute(self):
        """
        Add a new attribute/value row to the config table.
        :return: None;
        """
        table_row_count = self.ownConfigTable.rowCount()
        if table_row_count < 1:
            self.ownConfigTable.setColumnCount(2)
        self.ownConfigTable.setRowCount(table_row_count+1)

    def delete_attribute(self):
        """
        Delete the selected attribute row data.
        :return: None;
        """
        for item in self.ownConfigTable.selectedItems():
            item.setText('')

    def apply_config(self):
        """
        Create or update the entity's configuration with the attributes and values provided in its config table.
        :return: An info message window;
        """
        new_config_data = {}
        for i in range(0, self.ownConfigTable.rowCount()):
            attribute = self.ownConfigTable.item(i, 0)
            value = self.ownConfigTable.item(i, 1)
            if attribute and value:
                value_data = value.text()
                # Check for floating point values:
                if value_data.replace('.', '').isdigit():
                    value_data = float(value.text())
                new_config_data[attribute.text()] = value_data

        # Find existing config entity, otherwise create a new one:
        own_config = self.db_api.get_config(entity=self.entity)
        if own_config:
            self.db_api.update_config(own_config, new_config_data)
        else:
            self.db_api.configure(self.entity, new_config_data)
        return info_message(self, 'Configuration rules successfully applied!')

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.applyBtn.clicked.connect(self.apply_config)
        self.addAttributeBtn.clicked.connect(self.add_attribute)
        self.deleteSelectedBtn.clicked.connect(self.delete_attribute)


def show():
    """
    Show the configuration dialog.
    :return: None;
    """
    if environment_check('PIPER_TASK_UUID'):
        db_connection = DbConnection()
        current_task = db_connection.session.query(Task).filter_by(uuid=os.environ['PIPER_TASK_UUID']).first()
        cascading_config = CascadingConfig(entity=current_task.entity,
                                           parent=QApplication.activeWindow(),
                                           mode='display')
        cascading_config.exec_()
    else:
        raise EnvironmentError('Piper Cascading Config: There is no task currently being worked on!')
