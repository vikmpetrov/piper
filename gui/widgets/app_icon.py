# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import re
from piper.config import ICONS_PATH
from piper.gui.qt import Qt, QWidget, Signal, QLabel, QPixmap, QHBoxLayout

ICON_SIZE = 48


class AppIcon(QWidget):
    """
    An widget for representing app icons in the Piper launcher.
    """
    clicked = Signal(object)  # a signal emitted every time the icon gets clicked on;

    def __init__(self, app_name, app_version, app_bin, app_type='dcc', parent=None):
        super(AppIcon, self).__init__(parent)
        self.app_name = app_name
        self.app_version = app_version
        self.app_bin = app_bin  # the command to be executed when the icon is clicked on;
        self.app_type = app_type
        self.icon_label = QLabel()
        self.icon_label.setFixedSize(ICON_SIZE, ICON_SIZE)
        self.greyscale_pixmap = QPixmap('')
        self.version_label = QLabel('{0} {1}'.format(app_name.title(), self.app_version).replace('_', ' '))
        main_layout = QHBoxLayout()
        main_layout.setContentsMargins(10, 10, 10, 10)
        main_layout.addWidget(self.icon_label)
        main_layout.addWidget(self.version_label)
        self.setLayout(main_layout)
        self.isPressed = False
        self._setup_data()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        app_name_clean = '_'.join(re.findall("[a-zA-Z]+", self.app_name))
        highlight_icon_name = '{}.png'.format(app_name_clean)
        greyscale_icon_name = '{}-greyscale.png'.format(app_name_clean)
        self.greyscale_pixmap = QPixmap(os.path.abspath(os.path.join(ICONS_PATH, greyscale_icon_name)))
        self.icon_label.setPixmap(self.greyscale_pixmap)
        self.highlight_pixmap = QPixmap(os.path.abspath(os.path.join(ICONS_PATH, highlight_icon_name)))

    def enterEvent(self, event):
        """
        An override method for hovering on the icon.
        :param event: a Qt event;
        :return: None;
        """
        self.setCursor(Qt.PointingHandCursor)
        self.icon_label.setPixmap(self.highlight_pixmap)
        QWidget.enterEvent(self, event)

    def leaveEvent(self, event):
        """
        An override method for hovering off the icon.
        :param event: a Qt event;
        :return: None;
        """
        self.setCursor(Qt.ArrowCursor)
        self.icon_label.setPixmap(self.greyscale_pixmap)
        QWidget.leaveEvent(self, event)

    def mousePressEvent(self, event):
        """
        An override method for pressing the mouse.
        :param event: a Qt event;
        :return: None;
        """
        self.isPressed = True

    def mouseReleaseEvent(self, event):
        """
        An override method for releasing the mouse.
        :param event: a Qt event;
        :return: None;
        """
        if self.isPressed and event.button() == Qt.LeftButton:
            self.clicked.emit(self)
        self.isPressed = False
