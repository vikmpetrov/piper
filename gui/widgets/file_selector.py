# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.gui.qt import QListWidget, QListWidgetItem, QWidget, load_qt_ui
from piper.gui.utils import error_message, browse_file_dialog
from piper.api.logger import get_logger


class FileDrop(QListWidget):
    """
    File drag-and-drop list widget.
    """

    def __init__(self, parent):
        super(FileDrop, self).__init__(parent)
        self.parent = parent
        self.files = []  # to store file paths;
        self.extensions = []  # to store file extensions;
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        for url in event.mimeData().urls():
            path = url.toLocalFile()
            if os.path.isfile(path):
                self.add_item(file_name=path)
            else:
                return error_message(parent=self.parent, msg='File {} is invalid.'.format(path))

    @staticmethod
    def _file_sequence_check(file_path):
        """
        Check if the given file path is part of a sequence.
        :param file_path: the (str) file path;
        :return: the path with hash tag padding if it's a sequence, otherwise the original file path;
        """
        file_components = file_path.split('.')
        if len(file_components) > 2:
            index = file_components[-2]
            if index.isdigit():
                file_path = file_path.replace('.{}.'.format(index), '.####.')
        return file_path

    def add_item(self, file_name):
        extension = file_name.split('.')[-1]
        if extension in self.extensions:
            return error_message(parent=self.parent, msg='File of the same type {} already added.'.format(
                extension))
        file_name = self._file_sequence_check(file_path=file_name)
        if file_name in self.files:
            return error_message(parent=self.parent, msg='File {} added already, aborting.'.format(file_name))
        self.files.append(file_name)
        self.extensions.append(file_name.split('.')[-1])
        self.addItem(QListWidgetItem(str(file_name)))


class FileSelector(QWidget):
    """
    An widget for selecting files, both via drag and drop and browsing.
    """

    def __init__(self, parent=None):
        super(FileSelector, self).__init__(parent)
        self.logger = get_logger('Piper File Selector')
        load_qt_ui('file_selector.ui', self)
        self.parent = parent
        self.file_drop = FileDrop(parent=self)
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """
        self.fileDropLayout.addWidget(self.file_drop)

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.browseBtn.clicked.connect(self.browse_file)
        self.removeBtn.clicked.connect(self.remove_selected)

    def browse_file(self):
        for file_name in browse_file_dialog(parent=self):
            self.file_drop.add_item(file_name=file_name)

    def remove_selected(self):
        for item in self.file_drop.selectedItems():
            self.file_drop.takeItem(self.file_drop.row(item))
            file_name = item.text()
            self.file_drop.files.remove(file_name)
            self.file_drop.extensions.remove(file_name.split('.')[-1])
