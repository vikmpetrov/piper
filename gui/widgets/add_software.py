# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
from piper.config import ICONS_PATH, STUDIO_CONFIG_FILE, STUDIO_CONFIG
from piper.gui.qt import QDialog, QIcon, load_qt_ui
from piper.gui.utils import info_message, error_message
from piper.api.logger import get_logger
from piper.api.utils import read_json, write_json


class AddSoftware(QDialog):
    """
    A widget used to add a new software configuration to the studio config.
    """

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.logger = get_logger('Piper Add Software')
        load_qt_ui('add_software.ui', self)
        self.parent = parent
        self.move(self.parent.pos())  # Move this widget to where its parent window is.
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
        self.platform_data = {
            'win32': {'root': self.windowsRootLineEdit, 'bin': self.windowsBinLineEdit},
            'linux2': {'root': self.linuxRootLineEdit, 'bin': self.linuxBinLineEdit},
            'darwin': {'root': self.osxRootLineEdit, 'bin': self.osxBinLineEdit},
        }
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        if sys.platform == 'win32':
            self.platformTabs.setCurrentIndex(0)
        elif sys.platform == 'linux2':
            self.platformTabs.setCurrentIndex(1)
        else:
            self.platformTabs.setCurrentIndex(2)


    def add_software(self):
        """
        Update studio config file with new software data.
        :return: None;
        """
        software = self.nameLineEdit.text()
        if not software:
            return error_message(parent=self, msg='Invalid software name provided.')

        studio_config = read_json(STUDIO_CONFIG_FILE)
        if software.upper() in STUDIO_CONFIG.data['SOFTWARE']:
            return error_message(parent=self, msg='{} already exists in studio configuration.'.format(software.upper()))

        default_version = self.versionLineEdit.text()

        for system in self.platform_data:
            software_data = {
                            "BIN": self.platform_data[system]['bin'].text(),
                            "DEFAULT_VERSION": default_version,
                            "ROOT": self.platform_data[system]['root'].text()
                            }
            studio_config[system]['SOFTWARE'][software.upper()] = software_data
        write_json(studio_config, STUDIO_CONFIG_FILE)
        STUDIO_CONFIG.reload()
        info_message(parent=self, msg='Studio configuration updated with {}!'.format(software))
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.addBtn.clicked.connect(self.add_software)
        self.closeBtn.clicked.connect(self.close)
