# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from getpass import getuser
from piper.config import ICONS_PATH
from piper.gui.qt import QDialog, QIcon, QDateTime, load_qt_ui
from piper.gui.utils import info_message, error_message, set_combo_box_text
from piper.api.database_api import DbApi
from piper.api.logger import get_logger


class CreateTask(QDialog):
    """
    A widget used to create work file resources in the database.
    """
    def __init__(self, parent, project, entity, edit_mode=False):
        QDialog.__init__(self, parent)
        self.logger = get_logger('Piper Create Task')
        load_qt_ui('create_task.ui', self)
        self.parent = parent
        self.project = project
        self.entity = entity
        self.edit_mode = edit_mode
        self.move(self.parent.pos())  # Move this widget to where its parent window is.
        self.db_api = DbApi()
        self._data = {
            'artists': {},
            'pipeline_steps': {},
            'statuses': {}
        }  # To store label-to-entity references.
        self._setup_data()
        self._connect_widget_cmds()
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
        self.setWindowTitle('Piper Create Task {}'.format(__version__))
        if edit_mode:
            self.setWindowTitle('Piper Update Task')
            self.createTaskBtn.setText('Update task')

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.projectLabel.setText('Project: {}'.format(self.project.name))
        self.entityLabel.setText('{0}: {1}'.format(self.entity.type, self.entity.name))
        artists = self.db_api.get_entities(entity='Artist', filtered=True)
        for artist in artists:
            self.artistComboBox.addItem(artist.name)
            self._data['artists'][artist.name] = artist
        set_combo_box_text(combo_box=self.artistComboBox, text=getuser())
            
        self.db_api.filters['level'] = self.entity.type.lower()
        pipeline_steps = self.db_api.get_entities(entity='PipelineStep', filtered=True)
        for pipeline_step in pipeline_steps:
            self._data['pipeline_steps'][pipeline_step.label] = pipeline_step
            self.pipelineStepComboBox.addItem(pipeline_step.label)
        
        self.startDateEdit.setDateTime(QDateTime.currentDateTime())
        self.dueDateEdit.setDateTime(QDateTime.currentDateTime().addDays(7))
        self.db_api.filters = {'applies_to': 'Task'}
        for status in reversed(self.db_api.get_entities(entity='Status', filtered=True)):
            self.statusComboBox.addItem(status.label)
            self._data['statuses'][status.label] = status
        if self.edit_mode:
            self.taskNameLineEdit.setText(self.parent.selected_task.name)
            self.pipelineStepComboBox.setCurrentIndex([step.label for step in pipeline_steps].index(
                                                      self.parent.selected_task.pipeline_step.label))
            set_combo_box_text(combo_box=self.statusComboBox, text=self.parent.selected_task.status.label)
            self.descriptionLineEdit.setText(self.parent.selected_task.description)
            self.artistComboBox.setCurrentIndex(artists.index(self.parent.selected_task.artist))
            self.startDateEdit.setDateTime(QDateTime(self.parent.selected_task.start_date))
            self.dueDateEdit.setDateTime(QDateTime(self.parent.selected_task.end_date))
            self.commentLineEdit.setText(self.parent.selected_task.comment)

    def create_task(self):
        """
        Create a new task based on the user's input.
        :return: None;
        """
        task_name = self.taskNameLineEdit.text()
        if not task_name:
            return error_message(parent=self, msg='Task name has not been set!')
        shot = None
        asset = None
        pipeline_step = self._data['pipeline_steps'][self.pipelineStepComboBox.currentText()]
        if pipeline_step.level == 'asset':
            asset = self.entity
        else:
            shot = self.entity
        task = self.db_api.create_entity(entity='Task',
                                         name=task_name,
                                         project=self.project,
                                         shot=shot,
                                         asset=asset,
                                         artist=self._data['artists'][self.artistComboBox.currentText()],
                                         pipeline_step=pipeline_step,
                                         start_date=self.startDateEdit.date().toPython(),
                                         end_date=self.dueDateEdit.date().toPython(),
                                         status=self._data['statuses'][self.statusComboBox.currentText()],
                                         description=self.descriptionLineEdit.text(),
                                         comment=self.commentLineEdit.text())
        if task:
            msg = '{0} task {1} created!'.format(pipeline_step.label, task_name)
            info_message(parent=self, msg=msg)

        else:
            msg = 'Task {0} for project {1}, {2} {3} already exists!'.format(task_name,
                                                                             self.project.name,
                                                                             self.entity.type.lower(),
                                                                             self.entity.name)
            error_message(parent=self, msg=msg)
        self.close()

    def update_task(self):
        """
        Update the currently selected task based on the input provided in the UI.
        :return: None;
        """
        name = self.taskNameLineEdit.text()
        if not name:
            return error_message(parent=self, msg='Please provide a valid task name!')
        self.parent.selected_task.name = name
        self.parent.selected_task.label = self.taskNameLineEdit.text()
        self.parent.selected_task.artist = self._data['artists'][self.artistComboBox.currentText()]
        self.parent.selected_task.pipeline_step = self._data['pipeline_steps'][self.pipelineStepComboBox.currentText()]
        self.parent.selected_task.start_date = self.startDateEdit.date().toPython()
        self.parent.selected_task.end_date = self.dueDateEdit.date().toPython()
        self.parent.selected_task.status = self._data['statuses'][self.statusComboBox.currentText()]
        self.parent.selected_task.description = self.descriptionLineEdit.text()
        self.parent.selected_task.comment = self.commentLineEdit.text()
        self.db_api.publish(self.parent.selected_task)
        msg = 'Task {0} (ID: {1}) has been updated!'.format(self.parent.selected_task.name,
                                                            self.parent.selected_task.id)
        info_message(parent=self, msg=msg)
        self.close()


    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        if self.edit_mode:
            self.createTaskBtn.clicked.connect(self.update_task)
        else:
            self.createTaskBtn.clicked.connect(self.create_task)
