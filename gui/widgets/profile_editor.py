# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.config import ICONS_PATH
from piper.gui.qt import Qt, QDialog, QStandardItemModel, QStandardItem, QHBoxLayout, QLabel, QLineEdit, load_qt_ui, \
    QIcon
from piper.gui.utils import info_message, error_message, warning_message, set_combo_box_text
from piper.api.database_api import DbApi
from piper.api.utils import read_json, write_json
from piper.api.logger import get_logger
from piper.config import STUDIO_CONFIG, MODULES_PATH_TEMPLATE, PROFILES_PATH

CHECK_STATE_DICT = {Qt.Checked: True, Qt.Unchecked: False}  # Qt check state to boolean reference pairs;
PROFILES_HEADER = ['Software', 'Version', 'Type']


class ProfileEditor(QDialog):
    """
    A widget used for creating and editing software profiles.
    """
    def __init__(self, parent, profile_name=None, profile_file=None, mode='edit'):
        super(ProfileEditor, self).__init__()
        self.parent = parent
        load_qt_ui('profile_editor.ui', self)
        self.logger = get_logger('Piper Profile Editor')
        self.mode = mode  # the profile editor mode - 'create' or 'edit';
        self.profile_name = profile_name
        self.profile_file = os.path.join(PROFILES_PATH, '{0}.json'.format(profile_name))
        self.profile_data = dict()
        # Read the existing profile data if in 'edit' mode:
        if profile_file and self.mode == 'edit':
            self.profile_data = read_json(profile_file)
        self.item_data = dict()
        self.startup_file_widgets = dict()
        self.db_api = DbApi()
        self._setup_data()
        self._connect_widget_cmds()
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))
        self.setWindowTitle('Piper Profile Editor {}'.format(__version__))
        self.move(self.parent.pos())

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        if self.mode == 'edit':
            self.profileNameLineEdit.setText(self.profile_name)
        else:
            self.renameBtn.setHidden(True)

        self.item_model = QStandardItemModel()
        self.profileTreeView.setModel(self.item_model)
        self.refresh_tree_data()
        if self.profile_data:
            self.startupFilesDirectoryLineEdit.setText(self.profile_data['startup_directory'])
        else:
            self.startupFilesDirectoryLineEdit.setText(os.path.join(self.parent.profiler.profiles_path, 'startup'))

    def refresh_tree_data(self):
        """
        Reset the entire profile data tree.
        :return: None;
        """
        self.item_model.clear()
        self.item_model.setHorizontalHeaderLabels(PROFILES_HEADER)
        self.build_tree_data()
        self.profileTreeView.header().resizeSection(0, 200)
        self.profileTreeView.expandAll()

    def build_tree_data(self):
        """
        Build the profile data tree based on the studio configuration.
        :return: None;
        """
        # Append the tree root:
        tree_root = self.item_model.invisibleRootItem()
        software_item = self.append_data_tree_row(parent_item=tree_root, name='Software', row_type='', checkable=False)

        # Append all studio configured DCCs and their respective modules:
        for software in STUDIO_CONFIG.data['SOFTWARE']:
            check_state = Qt.Unchecked
            install_modules = []
            if self.profile_data and software.lower() in self.profile_data['software']:
                dcc_version = self.profile_data['software'][software.lower()]['version']
                install_modules = [m for m in self.profile_data['software'][software.lower()]['modules']
                                   if self.profile_data['software'][software.lower()]['modules'][m]]
                check_state = Qt.Checked
                startup_file = self.profile_data['software'][software.lower()]['startup_file']
            else:
                dcc_version = STUDIO_CONFIG.data['SOFTWARE'][software]['DEFAULT_VERSION']
                startup_file = ''

            dcc_item = self.append_data_tree_row(parent_item=software_item,
                                                 row_type='DCC',
                                                 row_version=dcc_version,
                                                 name=software.lower())
            dcc_item.setCheckState(check_state)
            modules_path = MODULES_PATH_TEMPLATE.format(dcc=software.lower())

            # Check for available DCC modules:
            if os.path.isdir(modules_path):
                for module in os.listdir(modules_path):
                    module_dir = os.path.join(modules_path, module)
                    module_file = [x for x in os.listdir(module_dir) if x.lower().endswith('.mod')]

                    if not module_file:
                        self.logger.error('No module file found in module %s.' % module_dir)
                        continue

                    module_file_path = os.path.join(modules_path, module, module_file[0])
                    with open(module_file_path, "r") as mod:
                        first_line = mod.readline()
                    try:
                        module_version = first_line.split(' ')[2]
                    except IndexError:
                        self.logger.error('Module file %s is not valid.' % module_file_path)
                        continue

                    module_item = self.append_data_tree_row(parent_item=dcc_item,
                                                            row_type='module',
                                                            row_version=module_version,
                                                            name=module)
                    if module in install_modules:
                        module_item.setCheckState(Qt.Checked)

            layout = QHBoxLayout()
            layout.addWidget(QLabel('{}:'.format(software.lower())))
            line_edit = QLineEdit()
            line_edit.setText(startup_file)
            layout.addWidget(line_edit)
            self.startupFilesLayout.addLayout(layout)
            self.startup_file_widgets[software.lower()] = line_edit

    def append_data_tree_row(self, parent_item, name, row_type, row_version='', checkable=True):
        """
        Append a software row to the profile data tree.
        :param parent_item: the row's parent Qt item;
        :param name: the (str) name of the main item;
        :param row_type: the (str) type of the row - 'DCC', 'module' or null;
        :param row_version: the (str) version of the software;
        :param checkable: a boolean to signify if the row can be checked or not;
        :return: a newly created QStandardItem;
        """
        main_item = QStandardItem(name)
        type_item = QStandardItem(row_type)
        version_item = QStandardItem(row_version)
        main_item.setEditable(False)
        type_item.setEditable(False)

        parent_item.appendRow([main_item, version_item, type_item])

        if checkable:
            main_item.setCheckable(True)

        self.item_data[name] = {}
        if row_type:
            parent_item_text = parent_item.text()
            self.item_data[parent_item_text][name] = {}
            self.item_data[parent_item_text][name]['main_item'] = main_item
            self.item_data[parent_item_text][name]['version_item'] = version_item

        return main_item

    def save_profile(self):
        """
        Save the profile data.
        :return: None;
        """
        profile_name = self.profileNameLineEdit.text()
        if not profile_name:
            error_message(self, 'Could not find a valid profile name.')
            return

        # Gather the profile data input in the UI:
        profile_contents = {'software': {}}
        for software in self.item_data['Software']:
            if CHECK_STATE_DICT[self.item_data['Software'][software]['main_item'].checkState()]:
                profile_contents['software'][software] = {}
                software_version = self.item_data['Software'][software]['version_item'].text()
                profile_contents['software'][software]['version'] = software_version
                profile_contents['software'][software]['modules'] = {}
                for module in self.item_data[software]:
                    install_module = CHECK_STATE_DICT[self.item_data[software][module]['main_item'].checkState()]
                    profile_contents['software'][software]['modules'][module] = install_module
                profile_contents['software'][software]['startup_file'] = self.startup_file_widgets[software].text()
        profile_contents['startup_directory'] = self.startupFilesDirectoryLineEdit.text()
        # Save the profile data in a JSON file:
        write_json(dict=profile_contents, path=self.profile_file)
        info_message(self, 'Profile saved!')
        self.logger.debug('Saved %s as profile "%s".' % (profile_contents, profile_name))
        self.parent.profile = profile_name
        self.close()

    def rename_profile(self):
        """
        Rename the current profile.
        :return: error message if no valid name given, warning message if the profile name was not changed,
        otherwise - None;
        """
        new_profile_name = self.profileNameLineEdit.text()
        if not new_profile_name:
            return error_message(parent=self, msg='No valid profile name set!')
        if new_profile_name == self.profile_name:
            return warning_message(parent=self, msg='Profile name already {}.'.format(new_profile_name))

        self.profile_name = new_profile_name
        new_profile_file = os.path.join(PROFILES_PATH, '{0}.json'.format(new_profile_name))
        os.rename(self.profile_file, new_profile_file)
        self.profile_file = new_profile_file
        self.parent.populate_profiles()
        set_combo_box_text(combo_box=self.parent.profilesComboBox, text=new_profile_name)

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.cancelBtn.clicked.connect(self.close)
        self.saveBtn.clicked.connect(self.save_profile)
        self.renameBtn.clicked.connect(self.rename_profile)
