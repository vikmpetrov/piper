# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.config import ICONS_PATH
from piper.gui.qt import QDialog, QIcon, QDateTime, QApplication, load_qt_ui
from piper.gui.utils import info_message, error_message
from piper.api.database_api import DbApi
from piper.api.logger import get_logger
from piper.api.utils import environment_check


class LogWork(QDialog):
    """
    A widget used to log time spent working on a task/resource.
    """

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.logger = get_logger('Piper Log Work')
        load_qt_ui('log_work.ui', self)
        self.parent = parent
        self.move(self.parent.pos())  # Move this widget to where its parent window is.
        self.db_api = DbApi()
        self.task = self.db_api.get_entity('Task', uuid=os.environ['PIPER_TASK_UUID'])
        self.setWindowIcon(QIcon(os.path.join(ICONS_PATH, 'piper_icon.png')))

        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.headerLabel.setText('Logging work for: {0} -> {1} -> {2} -> {3}'.format(self.task.project.label,
                                                                                     self.task.entity.sequence.label,
                                                                                     self.task.entity.label,
                                                                                     self.task.label))
        self.toTimeEdit.setDateTime(QDateTime.currentDateTime())
        self.fromTimeEdit.setDateTime(QDateTime.currentDateTime().addDays(-1))

    def log_work(self):
        """
        Create a new work log for the current task.
        :return: error message if unsuccessful, otherwise - None;
        """
        description = self.descriptionTextEdit.toPlainText()
        if not description:
            return error_message(parent=self, msg='Please describe the work you are logging in the description field!')

        work_log = self.db_api.create_entity(entity='WorkLog',
                                             name=self.toTimeEdit.dateTime().toPython().strftime('%Y-%m-%d %H:%M:%S'),
                                             task=self.task,
                                             project=self.task.project,
                                             time_from=self.fromTimeEdit.dateTime().toPython(),
                                             time_to=self.toTimeEdit.dateTime().toPython(),
                                             artist=self.db_api.get_current_artist(),
                                             description=description)

        info_message(parent=self,
                     msg='Logged {0} hours of work for {1} -> {2} -> {3} -> {4}!'.format(
                                                                                  work_log.duration.total_seconds()
                                                                                  //3600,
                                                                                  self.task.project.label,
                                                                                  self.task.entity.sequence.label,
                                                                                  self.task.entity.label,
                                                                                  self.task.label))
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.logBtn.clicked.connect(self.log_work)


def show():
    """
    Show the log work dialog.
    :return: None;
    """
    if environment_check('PIPER_TASK_UUID'):
        log_work = LogWork(parent=QApplication.activeWindow())
        log_work.exec_()
    else:
        raise EnvironmentError('Piper Log Work: There is no task currently being worked on!')
