# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from piper.api.logger import get_logger


def log_pyside_version(version, framework):
    """
    Log which PySide version is being run.
    :param version: the (str) version;
    :param framework: the (str) framework;
    :return: None;
    """
    logger = get_logger('Piper Qt')
    logger.debug('Running PySide %s, %s framework.' % (version, framework))


try:
    from PySide2 import __version__
    qt_framework = 'Qt 5.x'
    from PySide2.QtCore import *
    from PySide2.QtGui import *
    from PySide2.QtWidgets import *
    from PySide2.QtUiTools import *
    from pyside2uic import *
except ImportError:
    from PySide import __version__
    qt_framework = 'Qt 4.x'
    from PySide.QtCore import *
    from PySide.QtGui import *
    from PySide.QtUiTools import *
    from pysideuic import *

log_pyside_version(version=__version__, framework=qt_framework)


class UiLoader(QUiLoader):
    """
    A class for reading and loading a Qt UI form.
    """
    def __init__(self, base_instance):
        QUiLoader.__init__(self, base_instance)
        self.base_instance = base_instance

    def createWidget(self, class_name, parent=None, name=''):
        if parent is None and self.base_instance:
            return self.base_instance
        else:
            # create a new widget for child widgets
            widget = QUiLoader.createWidget(self, class_name, parent, name)
            if self.base_instance:
                setattr(self.base_instance, name, widget)
            return widget


def load_qt_ui(ui_file, base_instance=None):
    """
    Load a Qt UI form and return it as a widget.
    :param ui_file: the (str) path to the Qt UI form file;
    :param base_instance: a Qt base/parent UI class instance;
    :return: a Qt widget;
    """
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'forms', ui_file))
    loader = UiLoader(base_instance)
    widget = loader.load(file_path)
    QMetaObject.connectSlotsByName(widget)
    return widget

