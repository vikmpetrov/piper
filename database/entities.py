# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import re
from datetime import datetime
from sqlalchemy import Table, Column, ForeignKey, Integer, String, Text, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql.functions import func
from piper.config.db_config import PARENT_TO_CHILDREN
from piper.api.utils import get_current_user, generate_uuid
from piper.api.database_connection import DbConnection


class Base(object):
    """
    This base class will be linked to SQLAlchemy's declarative base.
    It contains key attributes and methods,
    which all entities in our database should have by default.
    """

    @declared_attr
    def __tablename__(cls):
        # Set the table name of this entity to its lowercase class name:
        return cls.__name__.lower()

    def __str__(self):
        # Get the name of this class as a string;
        return self.__class__.__name__

    def __repr__(self):
        # Get a human-readable string representation of this entity:
        return ' '.join(re.findall('[A-Z][^A-Z]*', self.__class__.__name__))

    id = Column(Integer, 
                primary_key=True, 
                autoincrement=True)  # the unique ID or number of this entity within its database table;
    name = Column(String)  # the name of the entity in the database;
    label = Column(String)  # the label represents the entity's name for display purposes, it's what the user will see;
    description = Column(Text)  # description of the entity, if applicable and/or required;
    link_entity = Column(String)  # the name of the entity that represents the corresponding table in another database;
    link_id = Column(Integer)  # a unique ID of the corresponding entity in another database, e.g. SG, if applicable;
    extra_info = Column(Text, default='{}')  # a dictionary to store any additional arbitrary data about the entity;
    comment = Column(String)  # any notes that the creator of this entity may have, if applicable;
    uuid = Column(String, default=generate_uuid)  # an identification string, unique in the entire database.
    parent_type = Column(String)  # the name of this entity's designated parent entity;
    parent_uuid = Column(String, default='')  # the unique identification string of the entity's parent;
    created = Column(DateTime(timezone=False), server_default=func.now())  # notes when this entity was created;
    updated = Column(DateTime(timezone=False), onupdate=func.now())  # notes when this entity was last updated;
    created_by = Column(String, default=get_current_user)  # the user name of this entity's creator;
    updated_by = Column(String, onupdate=get_current_user)  # the user name of whoever last updated this entity;

    _parent = None  # a private variable to store the entity's parent;

    def jsonify(self):
        """
        This method loops through all of the entity's attributes
        and returns them and their respective values as a dictionary.
        :return: key-value Python dictionary.
        """

        attributes = [attr for attr in dir(self) if not callable(getattr(self, attr)) 
                      and not attr.startswith('_')]
        attr_dict = dict()
        for attr in attributes:
            attr_dict[attr] = getattr(self, attr)
        return attr_dict

    @property
    def parent(self):
        """
        A method to get this entity's parent entity.
        :param session: the current database session;
        :return: a single entity, if it exists, otherwise None.
        """
        if not self._parent:
            if self.parent_uuid:
                db_connection = DbConnection()
                self._parent = db_connection.session.query(eval(self.parent_type)).filter_by(
                                                                                   uuid=self.parent_uuid).first()
        return self._parent

    @property
    def children(self):
        """
        A method to get this entity's children entities.
        :param session: the current database session;
        :return: a list of all of this entity's children-entities.
        """
        children = []
        if self.type in PARENT_TO_CHILDREN:
            for child in PARENT_TO_CHILDREN[self.type]:
                try:
                    db_connection = DbConnection()
                    children += db_connection.session.query(eval(child)).filter_by(parent_uuid=self.uuid).all()
                except Exception:
                    continue
        return children

    @property
    def type(self):
        """
        A property to easily access this entity's class name, which also represents its type.
        :return: the name of this entity's class as a string.
        """
        return self.__class__.__name__


# Get the declarative base class and assign it to the Base class:
Base = declarative_base(cls=Base)


class Status(Base):
    applies_to = Column(String)  # the name of the applicable entity that can have this status;
    is_active = Column(Boolean, default=True)


class StatusMixin(object):
    @declared_attr
    def status_id(cls):
        return Column(Integer, ForeignKey('status.id'), default=1)

    @declared_attr
    def status(cls):
        return relationship('Status')


class Department(Base, StatusMixin):
    """
    __tablename__ = 'department'
    #Setting a table name for each entity is redundant due to this attribute being declared in the inherited Base class.
    """

    artists = relationship('Artist', back_populates='department')
    email = Column(String)

    """
    # The following method is made obsolete by the inheritance of the Base entity class:
    def jsonify(self):
        return {
            'id': self.id,
            'name': self.name,
            'label': self.label,
            'status': self.status,
            'description': self.description,
            'extra_info': self.extra_info,
            'created': self.created,
            'updated': self.updated,
            'comment': self.comment
        }
    """


"""
The following class declaration code is pretty self-explanatory, no need for extensive documentation.
Creating all entity classes and give them entity-specific relationships and attributes as appropriate:
"""


class Artist(Base, StatusMixin):
    email = Column(String)
    department_id = Column(Integer, ForeignKey('department.id'))
    department = relationship('Department', back_populates='artists')
    resources = relationship('Resource', back_populates='artist')
    versions = relationship('Version', back_populates='artist')
    tasks = relationship('Task', back_populates='artist')
    files = relationship('File', back_populates='artist')
    is_administrator = Column(Boolean, default=False)
    worklogs = relationship('WorkLog', back_populates='artist')


class Project(Base, StatusMixin):
    assets = relationship('Asset', back_populates='project')
    sequences = relationship('Sequence', back_populates='project')
    shots = relationship('Shot', back_populates='project')
    resources = relationship('Resource', back_populates='project')
    versions = relationship('Version', back_populates='project')
    tasks = relationship('Task', back_populates='project')
    files = relationship('File', back_populates='project')
    worklogs = relationship('WorkLog', back_populates='project')


class Sequence(Base, StatusMixin):
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='sequences')
    shots = relationship('Shot', back_populates='sequence')


class AssetType(Base, StatusMixin):
    assets = relationship('Asset', back_populates='asset_type')


class Asset(Base, StatusMixin):
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='assets')

    asset_type_id = Column(Integer, ForeignKey('assettype.id'))
    asset_type = relationship('AssetType', back_populates='assets')

    tasks = relationship('Task', back_populates='asset')

    @property
    def resources(self):
        resources = []
        for task in self.tasks:
            resources += task.resources
        return resources

    @property
    def sequence(self):
        class AssetSequence(object):
            name = 'assets'
            label = 'assets'
        return AssetSequence()


class Shot(Base, StatusMixin):
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='shots')

    sequence_id = Column(Integer, ForeignKey('sequence.id'))
    sequence = relationship('Sequence', back_populates='shots')

    tasks = relationship('Task', back_populates='shot')

    @property
    def resources(self):
        resources = []
        for task in self.tasks:
            resources += task.resources
        return resources


class PipelineStep(Base, StatusMixin):
    tasks = relationship('Task', back_populates='pipeline_step')
    category = Column(String)
    level = Column(String)


class Task(Base, StatusMixin):
    pipeline_step_id = Column(Integer, ForeignKey('pipelinestep.id'))
    pipeline_step = relationship('PipelineStep', back_populates='tasks')

    asset_id = Column(Integer, ForeignKey('asset.id'))
    asset = relationship('Asset', back_populates='tasks')

    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='tasks')

    shot_id = Column(Integer, ForeignKey('shot.id'))
    shot = relationship('Shot', back_populates='tasks')

    resources = relationship('Resource', back_populates='task')

    start_date = Column(DateTime(timezone=False), default=None)
    end_date = Column(DateTime(timezone=False), default=None)

    artist_id = Column(Integer, ForeignKey('artist.id'))
    artist = relationship('Artist', back_populates='tasks')

    worklogs = relationship('WorkLog', back_populates='task')

    _entity = None

    @property
    def entity(self):
        if not self._entity:
            if self.asset:
                self._entity = self.asset
            else:
                self._entity = self.shot
        return self._entity

    @property
    def is_active(self):
        now = datetime.now()
        if (self.start_date <= now and self.end_date >= now) and self.status.is_active:
            return True
        return False

    @property
    def hours_logged(self):
        duration = 0
        for work_log in self.worklogs:
            duration += work_log.duration.total_seconds()//3600
        return duration


class Resource(Base, StatusMixin):
    source = Column(String)

    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='resources')

    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship('Task', back_populates='resources')

    versions = relationship('Version', back_populates='resource')

    author_id = Column(Integer, ForeignKey('artist.id'))
    artist = relationship('Artist', back_populates='resources')

    _asset = None
    _shot = None
    _entity = None
    _category = None

    @property
    def category(self):
        if not self._category:
            self._category = self.task.pipeline_step.category
        return self._category

    def _check_for_parent(self, parent_type):
        task_parent = self.task.parent
        if task_parent.type.lower() == parent_type.lower():
            return task_parent
        return None

    @property
    def asset(self):
        if not self._asset:
            self._asset = self._check_for_parent(parent_type='asset')
        return self._asset

    @property
    def shot(self):
        if not self._shot:
            self._shot = self._check_for_parent(parent_type='shot')
        return self._shot

    @property
    def entity(self):
        return self.asset or self.shot


version_dependency_table = Table('version_dependency', Base.metadata,
                                 Column('parent_id', Integer, ForeignKey('version.id'), primary_key=True),
                                 Column('child_id', Integer, ForeignKey('version.id'), primary_key=True))


class Version(Base, StatusMixin):
    files = relationship('File', back_populates='version')

    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='versions')

    resource_id = Column(Integer, ForeignKey('resource.id'))
    resource = relationship('Resource', back_populates='versions')

    author_id = Column(Integer, ForeignKey('artist.id'))
    artist = relationship('Artist', back_populates='versions')
    
    dependencies = relationship('Version',
                                secondary=version_dependency_table,
                                primaryjoin="Version.id == version_dependency.c.parent_id",
                                secondaryjoin="Version.id == version_dependency.c.child_id",
                                backref=backref('dependents'))
    
    @property
    def is_latest(self):
        latest_version = self.resource.versions[-1]
        if self.uuid == latest_version.uuid:
            return True
        return False


class File(Base, StatusMixin):
    format = Column(String)
    is_sequence = Column(Boolean, default=False)

    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='files')

    version_id = Column(Integer, ForeignKey('version.id'))
    version = relationship('Version', back_populates='files')

    author_id = Column(Integer, ForeignKey('artist.id'))
    artist = relationship('Artist', back_populates='files')

    _extension = None
    path = None  # to store the path to the file on disk;
    range = []  # to store frame range if the file is a sequence;

    @property
    def extension(self):
        if not self._extension:
            if self.is_sequence:
                self._extension = '.####.{}'.format(self.format)
            else:
                self._extension = '.{}'.format(self.format)
        return self._extension

    @property
    def full_name(self):
        return '{}{}'.format(self.name, self.extension)


class Config(Base, StatusMixin):
    config_rules = Column(Text, default='{}')


class WorkLog(Base):
    project_id = Column(Integer, ForeignKey('project.id'))
    project = relationship('Project', back_populates='worklogs')
    artist_id = Column(Integer, ForeignKey('artist.id'))
    artist = relationship('Artist', back_populates='worklogs')
    task_id = Column(Integer, ForeignKey('task.id'))
    task = relationship('Task', back_populates='worklogs')
    time_from = Column(DateTime(timezone=False), default=None)
    time_to = Column(DateTime(timezone=False), default=None)

    _duration = None

    @property
    def duration(self):
        if not self._duration:
            self._duration = self.time_to - self.time_from
        return self._duration
