# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
from getpass import getuser
from sqlalchemy import create_engine
from piper.database.entities import *
from piper.api.database_api import DbApi
from piper.config.db_config import DATABASE_URL, DATABASE_NAME, ASSET_TYPES, PIPELINE_STEP, PIPELINE_STEP_TO_CATEGORY, \
    STATUS, STATUS_IS_WORKABLE
# This module is used only once to initially create the database and populate it with some basic data.


def initialize():
    """
    Initialize the Piper database and create some fundamental entities.
    :return:
    """
    # Firstly, check if a piper database exists already:
    if os.path.isfile(os.path.join(os.path.dirname(__file__), DATABASE_NAME)):
        return

    # Create an engine to connect to the database and create all classes and tables catalogued in the declarative base:
    engine = create_engine(DATABASE_URL)
    Base.metadata.create_all(engine)

    # Create some basic entities:
    db_api = DbApi()
    for entity_type in sorted(STATUS):
        for status in sorted(STATUS[entity_type]):
            db_api.create_entity(entity='Status',
                                 name=STATUS[entity_type][status],
                                 label=status,
                                 applies_to=entity_type,
                                 is_active=STATUS_IS_WORKABLE[status])

    pipeline_department = db_api.create_entity(entity='Department', name='pipeline', label='Pipeline')
    it_department = db_api.create_entity(entity='Department', name='it', label='IT')

    # Create initial users:
    db_api.create_entity(entity='Artist',
                         name=getuser(),
                         department=pipeline_department,
                         email='{}@example.com'.format(getuser()),
                         is_administrator=True)

    db_api.create_entity(entity='Artist',
                         name='administrator',
                         department=it_department,
                         email='administrator@example.com',
                         is_administrator=True)

    for asset_type in ASSET_TYPES:
        db_api.create_entity(entity='AssetType', name=ASSET_TYPES[asset_type], label=asset_type)

    for level in PIPELINE_STEP:
        for pipeline_step in PIPELINE_STEP[level]:
            db_api.create_entity(entity='PipelineStep',
                                 level=level,
                                 name=PIPELINE_STEP[level][pipeline_step],
                                 label=pipeline_step,
                                 category=PIPELINE_STEP_TO_CATEGORY[pipeline_step])


if __name__ == '__main__':
    initialize()
