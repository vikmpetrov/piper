# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
import os
sys.path.append(os.environ['PIPER_INIT'])
sys.path.append(os.environ['PIPER_SITE_PACKAGES'])
import piper_setup
piper_setup.startup()
