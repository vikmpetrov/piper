# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
import os
sys.path.append(os.environ['PIPER_INIT'])
sys.path.append(os.environ['PIPER_SITE_PACKAGES'])
import nuke
from piper.config.dcc_config import NUKE_MENU_TEMPLATE
from piper.api.logger import get_logger
from piper.api.profiler import Profiler
from piper.api.dcc.nuke_api import open_file

logger = get_logger('Piper Setup - Nuke')
os.environ['PIPER_SOURCE'] = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))


def startup():
    """
    Run Piper start up methods - build the Piper menu, load the startup file and run Scene Builder.
    :return: None;
    """
    build_piper_menu()
    profiler = Profiler()
    profile_data = profiler.get_profile_data(profile_name=os.environ['PIPERPROFILE'])
    startup_file = os.path.join(profile_data['startup_directory'],
                                profile_data['software'][os.environ['PIPER_SOURCE']]['startup_file'])
    if os.path.isfile(startup_file):
        open_file(path_to_file=startup_file)
    else:
        logger.warn('Specified start up file {} is invalid.'.format(startup_file))
    exec (NUKE_MENU_TEMPLATE['piper']['Scene Builder'])


def build_piper_menu():
    """
    Create the Piper menu in Nuke and start the Piper Scene Builder.
    :return: None;
    """
    menu_root = nuke.menu('Nuke')

    try:
        piper_menu = menu_root.menu('piper')
        piper_menu.clearMenu()
    except Exception:
        piper_menu = menu_root.addMenu('piper')

    for action in sorted(NUKE_MENU_TEMPLATE['piper']):
        add_action(action=action, parent_menu=piper_menu, cmd=NUKE_MENU_TEMPLATE['piper'][action])

    piper_menu.addSeparator()

    """
    for department in sorted(NUKE_MENU_TEMPLATE['departments']):
        department_menu = piper_menu.addMenu(department)
        for action in NUKE_MENU_TEMPLATE['departments'][department]:
            add_action(action=action,
                       parent_menu=department_menu,
                       cmd=NUKE_MENU_TEMPLATE['departments'][department][action])
    """

    logger.info('Built Piper NUKE menu.')


def add_action(action, parent_menu, cmd):
    """
    Add a menu command.
    :param action: a (str) action name;
    :param parent_menu: the parent menu in the UI;
    :param cmd: a (str) action command script;
    :return: None;
    """
    return parent_menu.addCommand(action, cmd)
