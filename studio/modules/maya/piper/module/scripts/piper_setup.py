# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import sys
import os
sys.path.append(os.environ['PIPER_INIT'])
sys.path.append(os.environ['PIPER_SITE_PACKAGES'])
from maya import cmds
from piper.config.dcc_config import MAYA_MENU_TEMPLATE
from piper.api.logger import get_logger
from piper.api.profiler import Profiler
from piper.api.dcc.maya_api import open_file

logger = get_logger('Piper Setup - Maya')
os.environ['PIPER_SOURCE'] = os.path.basename(os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                                           '..', '..', '..')))

def startup():
    """
    Run Piper start up methods - build the Piper menu, load the startup file and run Scene Builder.
    :return: None;
    """
    build_piper_menu()
    profiler = Profiler()
    profile_data = profiler.get_profile_data(profile_name=os.environ['PIPERPROFILE'])
    startup_file = os.path.join(profile_data['startup_directory'],
                                profile_data['software'][os.environ['PIPER_SOURCE']]['startup_file'])
    if os.path.isfile(startup_file):
        open_file(path_to_file=startup_file)
    else:
        logger.warn('Specified start up file {} is invalid.'.format(startup_file))
    exec (MAYA_MENU_TEMPLATE['piper']['Scene Builder'])


def build_piper_menu():
    """
    Create the Piper menu in Maya and start the Piper Scene Builder.
    :return: None;
    """
    menu_root = 'MayaWindow|Piper'

    if menu_root not in cmds.lsUI(menus=True, long=True):
        menu_root = cmds.menu('Piper', parent='MayaWindow', tearOff=True)

    for action in sorted(MAYA_MENU_TEMPLATE['piper']):
        add_action(action=action, parent_menu=menu_root, cmd=MAYA_MENU_TEMPLATE['piper'][action])

    cmds.menuItem(divider=True)

    """
    for department in sorted(MAYA_MENU_TEMPLATE['departments']):
        department_menu = '{}|{}'.format(menu_root, department)
        if department_menu not in cmds.lsUI(menus=True, long=True):
            department_menu = cmds.menuItem(department, subMenu=True, parent=menu_root, tearOff=True)
            for action in MAYA_MENU_TEMPLATE['departments'][department]:
                add_action(action=action,
                           parent_menu=department_menu,
                           cmd=MAYA_MENU_TEMPLATE['departments'][department][action])
    """
    logger.info('Built Piper Maya menu.')


def add_action(action, parent_menu, cmd):
    """
    Add a menu command.
    :param action: a (str) action name;
    :param parent_menu: the parent menu in the UI;
    :param cmd: a (str) action command script;
    :return: None;
    """
    action_menu = '{}|{}'.format(parent_menu, action)
    if action_menu not in cmds.lsUI(menus=True, long=True):
        return cmds.menuItem(action,
                             subMenu=False,
                             parent=parent_menu,
                             tearOff=False,
                             command=cmd)
