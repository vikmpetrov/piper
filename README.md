# Piper - an out-of-the-box CGI pipeline software system

Piper is a basic, yet functional cross-platform VFX and/or 3D animation pipeline toolkit that provides fundamental data management solutions for CGI production. It serves both as a robust project manager and a link between its two currently supported DCC applications - Autodesk Maya and The Foundry's Nuke. This is the beta version of the Piper pipeline toolkit, which is production-ready for small teams working on open source, educational or non-profit projects. A commercial Piper version is available at http://www.piperpipeline.com. For more information and details on commercial support, feel free to contact me at viktor@piperpipeline.com, thanks!


Tested on Windows 10, macOS 10 and Ubuntu 18 running Maya 2017 and Nuke 11.
Thanks a lot for your interest in Piper!

## Getting Started

How to get Piper up and running on your machine/network.

### Prerequisites

This is what you need to have installed on your machine before running Piper:

- git, obviously (https://git-scm.com/downloads);
- Python, anything from version 2.7.14 to 3.4.x should be OK, Python3 recommended (https://www.python.org/downloads/);
- pip (https://pip.pypa.io/en/stable/installing/);
- virtualenv (https://virtualenv.pypa.io/en/stable/installation/);
- Qt 4.8 for running PySide (https://wiki.qt.io/Setting_up_PySide);

### Installing

How to set up Piper and its environment for the first time.

On Linux and OSX:

```
git clone git@gitlab.com:vikmpetrov/piper.git
cd piper
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

On Windows:

```
git clone git@gitlab.com:vikmpetrov/piper.git
cd piper
virtualenv venv
.\venv\Scripts\activate.bat
pip install -r requirements.txt
```

Additionally, you can specify a specific version of Python when running virtualenv, e.g:

```
virtualenv --python=/path/to/python venv
```

Note that you can install Piper anywhere you please, just make sure it's a shared directory if you plan to run Piper on a network as part of a team/studio.

## Deployment

After successful virtual environment set up, you can start the Piper Launcher from the relevant executable file:

* /piper/Piper-Launcher-Linux.sh (on Linux);
* /piper/Piper-Launcher-Windows.bat (on Windows);
* /piper/Piper-Launcher-OSX.command (on OSX);

## Workflow

Piper demo video: https://vimeo.com/293773792

All Piper apps and supported DCC apps should be run from the Piper Launcher.

Proposed initial steps for first-time Piper use:

1. Start Piper Launcher;
2. Run Piper Studio Config from Piper Launcher;
3. Use Piper Studio Config to create the initial departments and users/artists;
4. Use Piper Studio Config to configure the WORK_ROOT and PUBLISH_ROOT directories for file storage;
5. Use Piper Studio Config to configure your DCC software DEFAULT_VERSION, BIN and ROOT path as installed on your system;
6. Run Piper Project Manager from Piper Launcher;
7. Use Piper Project Manager to create and configure shows, assets, sequences and shots;
8. Run Piper Task Manager from Piper Launcher;
9. Use Piper Task Manager to create and assign tasks to artists;
10. (Optional) Run Piper Resource Ingestor from Piper Launcher;
11. (Optional) Use Piper Resource Ingestor to create new resources and ingest files as new versions;
12. Start your DCC application of choice from Piper Launcher;
13. Inside the DCC application, run the Piper Scene Builder from the Piper menu;
14. Use Piper Scene Builder to select a task to work on and create a new resource;
15. Create something in the DCC application and run Piper Publisher from the Piper menu to publish your work as a new resource version;
16. Run Piper Browser either from the Piper menu in the DCC application or from Piper Launcher to view all the data you've created;

## API

Using the Piper database API:

```
from piper.api.database_api import DbApi as PiperApi

piper_api = PiperApi()
print(dir(piper_api))

# Query entities example:
projects = piper_api.get_entities(entity='Project')
```

## Built With

* [PyCharm Community](https://www.jetbrains.com/pycharm/) - Python IDE
* [Qt Creator](https://www.qt.io/qt-features-libraries-apis-tools-and-ide/#ide) - Qt UI design IDE

## Contributing

Email me at vik.m.petrov@outlook.com if you wish to discuss contributing to this beta. Any feedback is also welcomed!

## Author

* **Viktor Petrov** - *Initial work* - [vikmpetrov](https://gitlab.com/vikmpetrov)

## License

This project is licensed under the GNU GPLv3 License - see the [LICENSE.md](LICENSE.md) file for further details.

## Acknowledgments

* Inspiration - Stephen Bell, NCCA
* Inspiration - Carlos Anguiano, Mango Pipeline

