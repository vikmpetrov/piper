# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os

# Database URL template:
DATABASE_NAME = 'piper.sqlite'
DATABASE_URL = 'sqlite:///{0}'.format(os.path.abspath(os.path.join(
                                                      os.path.dirname(__file__), '..', 'database', DATABASE_NAME)
                                                      ).replace('\\', '/'))

# A constant dictionary of standard statuses:
STATUS = {
        'All': {
            'Active': 'active',
            'Removed': 'removed'
        },
        'Task': {
            'Waiting to Start': 'waiting_to_start',
            'Ready': 'ready',
            'In Progress': 'in_progress',
            'Complete': 'complete',
            'Final': 'final',
            'On Hold': 'on_hold',
            'Omit': 'omit',
            'Review': 'review',
            'Could Be Better': 'cbb',
            'Fixing': 'fixing',
            'Approved': 'approved'
        }
}

# A constant dictionary of statuses and whether these represent an active (in use) state or not:
STATUS_IS_WORKABLE = {
    'Active': True,
    'Removed': False,
    'Waiting to Start': True,
    'Ready': True,
    'In Progress': True,
    'Complete': False,
    'Final': True,
    'On Hold': False,
    'Omit': False,
    'Review': True,
    'Could Be Better': True,
    'Fixing': True,
    'Approved': True
}

# A constant dictionary of standard categories:
CATEGORY = {'3D': '3d',
            '2D': '2d'}

# A constant dictionary of standard pipeline steps:
PIPELINE_STEP = {
    'shot': {
        'Concept': 'cpt',
        'Surface': 'srf',
        'Modeling': 'mod',
        'Digital Matte Painting': 'dmp',
        'Texture': 'txr',
        'Rigging': 'rig',
        'Animation': 'ani',
        'Effects': 'efx',
        'Character FX': 'cfx',
        'Layout': 'lay',
        'Lighting': 'lgt',
        'Compositing': 'cmp',
        'Slapcomp': 'slp',
        'Matchmove': 'mmv',
        'Environment': 'env',
        'Roto': 'rot',
        'Paint': 'pnt',
        'Plate': 'plt'
    },
    'asset': {
        'Concept': 'cpt',
        'Surface': 'srf',
        'Modeling': 'mod',
        'Digital Matte Painting': 'dmp',
        'Texture': 'txr',
        'Rigging': 'rig',
        'Animation': 'ani',
        'Effects': 'efx',
        'Character FX': 'cfx',
        'Lighting': 'lgt',
        'Lookdev': 'ldv',
        'Compositing': 'cmp'
    }
}

# A constant dictionary of pipeline step to category pairs:
PIPELINE_STEP_TO_CATEGORY = {
                                'Concept': CATEGORY['2D'],
                                'Surface': CATEGORY['3D'],
                                'Modeling': CATEGORY['3D'],
                                'Digital Matte Painting': CATEGORY['2D'],
                                'Texture': CATEGORY['2D'],
                                'Rigging': CATEGORY['3D'],
                                'Animation': CATEGORY['3D'],
                                'Effects': CATEGORY['3D'],
                                'Character FX': CATEGORY['3D'],
                                'Layout': CATEGORY['3D'],
                                'Lighting': CATEGORY['3D'],
                                'Lookdev': CATEGORY['3D'],
                                'Compositing': CATEGORY['2D'],
                                'Slapcomp': CATEGORY['2D'],
                                'Matchmove': CATEGORY['3D'],
                                'Environment': CATEGORY['3D'],
                                'Roto': CATEGORY['2D'],
                                'Paint': CATEGORY['2D'],
                                'Plate': CATEGORY['2D']
}

"""
# A constant dictionary of pipeline step to file formats pairs:
PIPELINE_STEP_TO_FILE_TYPE = {
                                'Concept': '*.exr;*.tif;*.jpg',
                                'Surface': '*.ma',
                                'Modeling': '*.ma',
                                'Digital Matte Painting': '*.exr;*.tif;*.jpg',
                                'Texture': '*.exr;*.tif;*.jpg',
                                'Rigging': '*.ma',
                                'Animation': '*.ma',
                                'Effects': '*.ma',
                                'Character FX': '*.ma',
                                'Layout': '*.ma',
                                'Lighting': '*.ma',
                                'Lookdev': '*.ma',
                                'Compositing': '*.nknc',
                                'Slapcomp': '*.nknc',
                                'Matchmove': '*.ma',
                                'Environment': '*.ma',
                                'Roto': '*.nknc',
                                'Paint': '*.nknc',
                                'Plate': '*.exr;*.jpg;*.mov'
}
"""

# A constant dictionary of standard asset types:
ASSET_TYPES = {'Character': 'chr',
               'Effects': 'efx',
               'Environment': 'env',
               'Prop': 'prp',
               'Vehicle': 'veh'}

# A template of parent-children relationships:
PARENT_TO_CHILDREN = {
            'Department': ['Artist'],
            'Project': ['Sequence', 'Asset'],
            'Sequence': ['Shot'],
            'Asset': ['Task'],
            'Shot': ['Task'],
            'Task': ['Resource', 'WorkLog'],
            'Resource': ['Version'],
            'Version': ['File']
}

# A template for on-disk VFX path generation:
VFX_PATH = {
    'publish':
        {
                'File': {
                    'append': '"{0}{1}".format(entity.name, entity.extension)',
                    'go_to': '"Version"',
                    'set_entity': 'entity.version'
                },
                'Version': {
                    'append': 'entity.name',
                    'go_to': '"Resource"',
                    'set_entity': 'entity.resource'
                },
                'Resource': {
                    'append': 'entity.name',
                    'go_to': '"ResourceType"',
                    'set_entity': 'entity'
                },
                'ResourceType': {
                    'append': 'entity.task.pipeline_step.name',
                    'go_to': '"Publish"',
                    'set_entity': 'entity'
                },
                'Publish': {
                    'append': '"publish"',
                    'go_to': '"ResourceCategory"',
                    'set_entity': 'entity'
                },
                'ResourceCategory': {
                    'append': 'entity.category',
                    'go_to': 'entity.entity.parent.type',
                    'set_entity': 'entity.entity.parent'
                },
                'Shot': {
                    'append': 'entity.name',
                    'go_to': '"Sequence"',
                    'set_entity': 'entity.sequence'
                },
                'Sequence': {
                    'append': 'entity.name',
                    'go_to': '"Project"',
                    'set_entity': 'entity.project'
                },
                'Asset': {
                    'append': '"{0}/{1}".format(entity.sequence.name, entity.name)',
                    'go_to': '"Project"',
                    'set_entity': 'entity.project'
                },
                'Project': {
                    'append': 'entity.name',
                    'go_to': '0',
                    'set_entity': '0'
                }
        },

    'work':
        {
                'File': {
                    'append': '"{0}{1}".format(entity.name, entity.extension)',
                    'go_to': '"Version"',
                    'set_entity': 'entity.version'
                },
                'Version': {
                    'append': 'entity.name',
                    'go_to': '"Resource"',
                    'set_entity': 'entity.resource'
                },
                'Resource': {
                    'append': 'entity.name',
                    'go_to': '"ResourceType"',
                    'set_entity': 'entity'
                },
                'ResourceType': {
                    'append': 'entity.task.pipeline_step.name',
                    'go_to': '"User"',
                    'set_entity': 'entity'
                },
                'User': {
                    'append': 'USER',
                    'go_to': '"Workspace"',
                    'set_entity': 'entity'
                },
                'Workspace': {
                    'append': '"workspace"',
                    'go_to': '"ResourceCategory"',
                    'set_entity': 'entity'
                },
                'ResourceCategory': {
                    'append': 'entity.category',
                    'go_to': 'entity.entity.parent.type',
                    'set_entity': 'entity.entity.parent'
                },
                'Shot': {
                    'append': 'entity.name',
                    'go_to': '"Sequence"',
                    'set_entity': 'entity.sequence'
                },
                'Sequence': {
                    'append': 'entity.name',
                    'go_to': '"Project"',
                    'set_entity': 'entity.project'
                },
                'Asset': {
                    'append': '"{0}/{1}".format(entity.sequence.name, entity.name)',
                    'go_to': '"Project"',
                    'set_entity': 'entity.project'
                },
                'Project': {
                    'append': 'entity.name',
                    'go_to': '0',
                    'set_entity': '0'
                }
        }
    }

# A template for naming version files:
VERSION_FILE_NAME_TEMPLATE = '{PROJECT}_{SEQUENCE}_{SHOT}_{RESOURCE}'
