# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os
import sys
import platform
from shutil import copyfile

# Set static global variables:
PIPER_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), '..')).replace('\\', '/')
sys.path.append(os.path.abspath(os.path.join(PIPER_ROOT, '..')))

ICONS_PATH = os.path.join(PIPER_ROOT, 'gui', 'images', 'icons')

MODULES_PATH_TEMPLATE = PIPER_ROOT + '/studio/modules/{dcc}'
PROFILES_PATH = PIPER_ROOT + '/studio/profiles'

# Specify each supported DCC's modules environment variable name:
DCC_MODULE_VARIABLE = {
    "maya": "MAYA_MODULE_PATH",
    "nuke11.2": "NUKE_PATH"
}

# Setting Piper environment variables:
os.environ['PIPER_ROOT'] = PIPER_ROOT
os.environ['PIPER_INIT'] = os.path.abspath(os.path.join(PIPER_ROOT, '..'))
os.environ['PIPER_VENV'] = os.path.join(PIPER_ROOT, 'venv').replace('\\', '/')
os.environ['PIPER_SOURCE'] = 'standalone'

# Set Python executable based on the OS:
if sys.platform == "win32":
    os.environ['PIPER_PYTHON'] = os.path.join(os.environ['PIPER_VENV'], 'Scripts', 'python.exe')
    os.environ['PIPER_PYTHONW'] = os.path.join(os.environ['PIPER_VENV'], 'Scripts', 'pythonw.exe')
    os.environ['PIPER_SITE_PACKAGES'] = os.path.join(os.environ['PIPER_VENV'], 'Lib', 'site-packages')
else:
    os.environ['PIPER_PYTHON'] = os.path.join(os.environ['PIPER_VENV'], 'bin', 'python')
    os.environ['PIPER_PYTHONW'] = os.path.join(os.environ['PIPER_VENV'], 'bin', 'pythonw')
    os.environ['PIPER_SITE_PACKAGES'] = os.path.join(os.environ['PIPER_VENV'],
                                                     'lib',
                                                     '{}'.format([x for x in os.listdir(
                                                         os.path.join(os.environ['PIPER_VENV'], 'lib'))
                                                                  if not x.startswith('.')][0]),
                                                     'site-packages')

# Get studio configuration:
from piper.api.utils import read_json
DEFAULT_STUDIO_CONFIG_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), 'default_studio_config.json'))
STUDIO_CONFIG_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), 'studio_config.json'))
# If there is no existing studio config file - copy the default studio config as the studio config:
if not os.path.isfile(STUDIO_CONFIG_FILE):
    copyfile(DEFAULT_STUDIO_CONFIG_FILE, STUDIO_CONFIG_FILE)

class StudioConfig:
    """
    A class to fetch and store studio configuration data.
    """
    def __init__(self):
        self.all_data = {}  # to hold configuration data for all platforms;
        self.data = {}  # to hold configuration data for the current platforms;
        self.reload()

    def reload(self):
        self.all_data = read_json(STUDIO_CONFIG_FILE)
        self.data = self.all_data[sys.platform]

STUDIO_CONFIG = StudioConfig()
