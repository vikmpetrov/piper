# -*- coding: utf-8 -*-
__author__ = "Viktor Petrov"
__copyright__ = "Copyright (C) 2018 Viktor Petrov"
__email__ = "vik.m.petrov@outlook.com"
__license__ = "GNU GPLv3"
__version__ = "0.1.0"

import os

DCC_API_MODULE_PATH = os.path.abspath(os.path.join(os.environ['PIPER_ROOT'], 'api', 'dcc'))  # path to the DCC APIs;

# The template for Maya menu creation:
MAYA_MENU_TEMPLATE = {
            'piper': {
                'Scene Builder': 'import piper.apps.scene_builder as piper_scene_builder\n'
                                 'piper_scene_builder.show(dcc="Maya")',
                'Browser': 'import piper.apps.browser as piper_browser; piper_browser.show()',
                'Snapshot': 'import piper.api.dcc.maya_api as dcc_api; dcc_api.snapshot()',
                'Publisher': 'import piper.apps.publisher as piper_publisher; \
                              piper_publisher.show(dcc="Maya")',
                'Log Work': 'import piper.gui.widgets.log_work as piper_log_work; piper_log_work.show()',
                'Cascading Config': 'import piper.gui.widgets.cascading_config as piper_cascading_config; \
                                     piper_cascading_config.show()'
            }
    }

# The template for NUKE menu creation:
NUKE_MENU_TEMPLATE = {
            'piper': {
                'Scene Builder': 'import piper.apps.scene_builder as piper_scene_builder\n'
                                 'piper_scene_builder.show(dcc="Nuke")',
                'Browser': 'import piper.apps.browser as piper_browser; piper_browser.show()',
                'Snapshot': 'import piper.api.dcc.nuke_api as dcc_api; dcc_api.snapshot()',
                'Publisher': 'import piper.apps.publisher as piper_publisher; \
                              piper_publisher.show(dcc="nuke")',
                'Log Work': 'import piper.gui.widgets.log_work as piper_log_work; piper_log_work.show()',
                'Cascading Config': 'import piper.gui.widgets.cascading_config as piper_cascading_config; \
                                     piper_cascading_config.show()'
            }
    }


# All supported DCC file associations:
DCC_FILE_EXTENTIONS = {
    'maya': ['ma', 'mb'],
    'nuke': ['nknc']
}


# DCC-category relationships:
DCC_TO_CATEGORY = {
    'maya': '3d',
    'nuke': '2d',
    'houdini': '3d',
    'photoshop': '2d',
    'mari': '2d'
}

# DCC-to-pipeline step relationships:
DCC_TO_PIPELINE_STEP = {
    'maya': ['Surface',
             'Modeling',
             'Rigging',
             'Animation',
             'Effects',
             'Character FX',
             'Lighting',
             'Matchmove',
             'Environment',
             'Layout',
             'Lookdev'],
    'nuke': ['Digital Matte Painting',
             'Compositing',
             'Slapcomp',
             'Roto',
             'Paint',
             'Plate'],
    'houdini': ['Effects',
                'Character FX'],
    'photoshop': ['Concept',
                  'Digital Matte Painting',
                  'Texture'],
    'mari': ['Texture']
}
